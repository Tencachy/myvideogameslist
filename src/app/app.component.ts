import { Component } from '@angular/core';
import { AUTHENTICATION_KEY } from './guards/auto-login.guard';
import { AuthenticationService } from './services/authentication.service';
import { LocalstorageService } from './services/localstorage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  isAuthenticated: boolean;
  username: string;
  dark_theme: boolean;
  public profileimage = '../assets/profile_light.png';

  constructor(
    private localStrg: LocalstorageService, 
    private authentication: AuthenticationService,
  ) 
  {
    this.isAuthenticated = false;
    this.checkDarkTheme();
  }

  logout(){
    this.authentication.logout();
  }

  checkAuthentication() {
    if( this.localStrg.get(AUTHENTICATION_KEY) === 'true'){
      this.isAuthenticated = true;
    } else {
      this.isAuthenticated = false;
    }
  }

  checkDarkTheme(){
    const preferDark=window.matchMedia('(prefers-color-schema:dark)');
    if(preferDark.matches){
      document.body.classList.toggle('dark');
    }
  }

  getUsername() {
    if(this.isAuthenticated){
      this.username = this.authentication.user.username;
    }
  }

  changeProfileImage() {
    if (this.dark_theme) {
      this.profileimage = '../assets/profile_dark.png'
    } else {
      this.profileimage = '../assets/profile_light.png'
    }
  }

  openSideMenu() {
    this.authentication.getUser();
    this.checkAuthentication();
    this.changeProfileImage();
    this.getUsername();
  }
}
