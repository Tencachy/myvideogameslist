import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';

export const AUTHENTICATION_KEY = 'authenticated';
import { LocalstorageService } from '../services/localstorage.service';

@Injectable({
  providedIn: 'root'
})
export class AutoLoginGuard implements CanLoad {

  constructor(
    private localStrg: LocalstorageService, 
    /*private storage: Storage, */
    private router: Router) {}

  async canLoad(): Promise<boolean> {
    if(this.localStrg.get(AUTHENTICATION_KEY) === 'true') {
      this.router.navigateByUrl('/home', {replaceUrl : true});
      return true;
    }else {
      return true;
    }
  }
}
