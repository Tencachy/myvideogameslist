import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';

import { AUTHENTICATION_KEY } from '../guards/auto-login.guard';
import { LocalstorageService } from '../services/localstorage.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {

  constructor(
    private localStrg: LocalstorageService, 
    /*private storage: Storage,*/
    private router: Router) {}
  
  async canLoad(): Promise<boolean> {
    if(this.localStrg.get(AUTHENTICATION_KEY) === 'true') {
      return true;
    }else {
      this.router.navigateByUrl('/login', {replaceUrl : true});
      return true;
    }
  }
}
