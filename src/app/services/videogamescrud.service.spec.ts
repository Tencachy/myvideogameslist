import { TestBed } from '@angular/core/testing';

import { VideogamescrudService } from './videogamescrud.service';

describe('VideogamescrudService', () => {
  let service: VideogamescrudService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VideogamescrudService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
