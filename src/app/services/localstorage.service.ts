import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  constructor() { }

  set(key: string, value: any) {
    try{
      localStorage.setItem(key, value);
    }catch (e) {
      console.log(e);
    }
  }

  get(key: string) {
    try{
      return localStorage.getItem(key);
    }catch (e) {
      console.log(e);
    }
  }
}
