import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Game } from '../models';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class VideogamescrudService {
  videogameName: string;
  firebaseRoute: string;
  videogamesListPlayed: Observable<any>;

  constructor(
    private auth: AuthenticationService, 
    private database: AngularFireDatabase, 
    private toastController: ToastController
  ) { }

  pushVideogame(videogame: Game, list: string) {
    if(this.auth.userID !== null && this.auth.userID!==undefined){
    let strings:string[]=['wantToPlay','played','playing'];
    strings.forEach((element,index)=>{
      if(element===list) strings.splice(index,1);
    })

    for(let listName of strings){
      this.removeSingleVideogame(listName,videogame.name)
    }
    this.videogameName = videogame.name;

      this.firebaseRoute = `Users/${this.auth.userID}/${list}/${this.videogameName}`;
      console.log(this.firebaseRoute);
  
      this.database.object(this.firebaseRoute).set({
         videogame
      }).then(() => {
        console.log("Game uploaded")
        this.gameUploadedToast(list);
      })
    }
  }

  async gameUploadedToast(list: string) {
    const toast = await this.toastController.create({
      message: `${this.videogameName} added to ${list}`,
      duration: 1500, 
      cssClass: "my-toast"
    });
    toast.present();
  }

  getVideogamesFromFirebase(list: string) {
    return this.videogamesListPlayed = this.database.list(`Users/${this.auth.userID}/${list}`).valueChanges();
  }

  getVideogameName(videogame: Game) {
    this.videogameName = videogame.name;
    console.log("CRUD " + this.videogameName)
  }

  removeSingleVideogame(list: string, name: string) {
    let gamePath = this.database.object(`Users/${this.auth.userID}/${list}/${name}`);
    gamePath.remove().then(() => {
      this.gameRemovedToast(name, list);
    });
  }

  async gameRemovedToast(name: string, list: string) {
    const toast = await this.toastController.create({
      message: `${name} removed from ${list}`,
      duration: 1500, 
      cssClass: "my-toast"
    });
    toast.present();
  }

}
