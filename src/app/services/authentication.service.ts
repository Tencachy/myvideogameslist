import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AUTHENTICATION_KEY } from '../guards/auto-login.guard';
import { LocalstorageService } from './localstorage.service';

export const LAST_USER_ID = 'userid';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  userID: string;
  isAuthenticated: boolean;
  user: any;

  constructor(
    private localStrg: LocalstorageService, 
    /*private storage: Storage, */
    private auth: AngularFireAuth,
    private router: Router,
    private database: AngularFireDatabase
  ) { }

  register(user) {
    if(user.email && user.password) {
      this.auth.createUserWithEmailAndPassword(user.email, user.password).then(
        (res) =>{
          console.log("Register");

          this.database.object('Users/' + res.user.uid).set({
            username: user.username,
            email: user.email,
            createdAt: Date.now()
          }).then(() => {
            this.setUserAuthenticatedTrue();
            this.router.navigateByUrl('/home');
          });
        }
      ).catch(e =>{
        console.log(e);
      })
    }
  }

  login(user) {
    if(user.email && user.password) {
      this.auth.signInWithEmailAndPassword(user.email, user.password).then(
        (res) =>{
          console.log("Login");
          this.setUserAuthenticatedTrue();
          /*this.isAuthenticated = true;*/
          this.getCurrentUserId();
          this.getUser();

          this.router.navigateByUrl('/home');
        }
      ).catch(e => {
        console.log(e);
      })
    }
  }

  logout(){
    this.auth.signOut().then(
      (res) =>{
        console.log("Logout");
        this.userID = null;
        this.setUserAuthenticatedFalse();
        /*this.isAuthenticated = false;*/
        
        this.router.navigateByUrl('/login');
      }
    ).catch(e =>{
      console.log(e)
    })
  }

  resetPassword(email){
    if(email){
      this.auth.sendPasswordResetEmail(email).then((res) => {
        console.log("Email reset password");
        this.router.navigateByUrl('/login');
      }).catch(e => {
        console.log(e)
      })
    }
  }

  getCurrentUserId(): string {
    if(this.localStrg.get(AUTHENTICATION_KEY) === 'true'){
      this.auth.user.subscribe((res) => {
        this.userID = res.uid;
        this.localStrg.set(LAST_USER_ID, this.userID)
        console.log("USER ID: " + this.userID)
      })
    }else{
      return null;
    }
  }

  async setUserAuthenticatedTrue() {
    await this.localStrg.set( AUTHENTICATION_KEY, 'true');
  }

  async setUserAuthenticatedFalse() {
    await this.localStrg.set( AUTHENTICATION_KEY, 'false');
  }

  getUser() {
    if(this.localStrg.get(AUTHENTICATION_KEY) === 'true'){
      this.database.object(`Users/${this.localStrg.get(LAST_USER_ID)}`).valueChanges().subscribe((res) => {
        this.user = res;
      })
    }
  }
}
