import { Injectable } from '@angular/core';
import{HttpClient, HttpParams} from '@angular/common/http'
import { forkJoin, from, Observable } from 'rxjs';
import{environment as env}from 'src/environments/environment'
import{APIResponse, Screenshots, Trailer} from 'src/app/models'
import{Game} from 'src/app/models'
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class GamesService {
  constructor(private gameService:HttpClient) { }

  getGameList(ordering:string,search?:string):Observable<APIResponse<Game>>{
    let params=new HttpParams().set('ordering',ordering);
    if(search){
      params=new HttpParams().set('ordering',ordering).set('search',search).set('search_exact','true');
    }

    return this.gameService.get<APIResponse<Game>>(`${env.BASE_URL}/games`,{params:params,})
  }


  getGameDetails(id:string):Observable<Game>{
    return this.gameService.get<Game>(`${env.BASE_URL}/games/`+id)
  }

  getGameScreenshots(id:string):Observable<APIResponse<Screenshots>>{
    return this.gameService.get<APIResponse<Screenshots>>(`${env.BASE_URL}/games/${id}/screenshots`)
  }

  getGameTrailers(id:string):Observable<APIResponse<Trailer>>{
    return this.gameService.get<APIResponse<Trailer>>(`${env.BASE_URL}/games/${id}/movies`)
  }

  getGamesByGenre(ordering:string,filter:string,name:string):Observable<APIResponse<Game>>{
    let params=new HttpParams().set('ordering',ordering).set(filter,name);
    return this.gameService.get<APIResponse<Game>>(`${env.BASE_URL}/games`,{params:params,})
  }

  getGamesByPlatform(ordering:string,name:number):Observable<APIResponse<Game>>{
    let params=new HttpParams().set('ordering',ordering)
    return this.gameService.get<APIResponse<Game>>(`${env.BASE_URL}/games?platforms=`+name,{params:params,})
  }
}
