export interface Game{
    id: number;
    background_image:string;
    name:string;
    released:string;
    metacritic_url:string;
    website:string;
    description:string;
    metacritic:number;
    genres:Array<Genre>;
    platforms:Array<Platforms>;
    parent_platforms:Array<ParentPlatform>;
    publishers:Array<Publishers>
    ratings:Array<Rating>;
    screenshots:Array<Screenshots>;
    trailers:Array<Trailer>;
    state: string;
    optionColorWantToPlay: string;
    optionColorPlaying: string;
    optionColorPlayed: string;
    rating: number;
}

export interface APIResponse<T>{
    results:Array<T>;
}

interface Genre{
    name:string;
}

interface ParentPlatform{
    platform:{name:string};
}

interface Publishers{
    name:string;
}

interface Platforms{
    name:string;
}

interface Rating{
    id:number;
    count:number;
    title:string;
}

export interface Screenshots{
    image:string;
}

export interface Trailer{
    preview:string;
    data:{
        max:string;
    };
}

