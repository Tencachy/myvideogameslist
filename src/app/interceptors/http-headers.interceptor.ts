import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest}from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable,throwError as observableThrowError } from "rxjs";

@Injectable()
export class HttpHeadersInterceptor implements HttpInterceptor{
    constructor(){}

    intercept(
        req:HttpRequest<any>,
        next:HttpHandler
    ):Observable<HttpEvent<any>>{
     req=req.clone({
         setHeaders:{
            "x-rapidapi-key": "f1c527b534msh9ca68b2c5480a0cp1296fbjsnda3aee070ee9",
            "x-rapidapi-host": "rawg-video-games-database.p.rapidapi.com",
         },
         setParams:{
             key: 'e60c45cc6e3d47c49ba21ee9eaed45d1'
         }
     });
     return next.handle(req)
    }
}