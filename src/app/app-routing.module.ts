import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { AutoLoginGuard } from './guards/auto-login.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'search/:game-search',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'detail/:id',
    loadChildren: () => import('./pages/detail/detail.module').then( m => m.DetailPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule),
    canLoad: [AutoLoginGuard]
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule), 
    canLoad: [AuthGuard]
  },
  {
    path: 'want-to-play',
    loadChildren: () => import('./pages/want-to-play/want-to-play.module').then( m => m.WantToPlayPageModule), 
    canLoad: [AuthGuard]
  },
  {
    path: 'currently-playing',
    loadChildren: () => import('./pages/currently-playing/currently-playing.module').then( m => m.CurrentlyPlayingPageModule), 
    canLoad: [AuthGuard]
  },
  {
    path: 'played',
    loadChildren: () => import('./pages/played/played.module').then( m => m.PlayedPageModule), 
    canLoad: [AuthGuard]
  },
  {
    path: 'top-games',
    loadChildren: () => import('./pages/top-games/top-games.module').then( m => m.TopGamesPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'forget-password',
    loadChildren: () => import('./pages/forget-password/forget-password.module').then( m => m.ForgetPasswordPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
