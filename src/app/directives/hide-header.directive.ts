import { Directive, OnInit, Input, Renderer2, HostListener, NgZone } from '@angular/core';
import { DomController } from '@ionic/angular';
import { HomePageModule } from '../pages/home/home.module';
import { HomePage } from '../pages/home/home.page';

@Directive({
  selector: '[appHideHeader]'
})
export class HideHeaderDirective implements OnInit {

  @Input('appHideHeader') header: any;

  private lastScrollTop: number = 0;
  private newOpacity: number = null;

  constructor(
    private renderer: Renderer2, 
    private domCtrl: DomController, 
    private home: HomePage,
    lc: NgZone
  ) {
    window.onscroll = () => {
      let st = window.pageYOffset;
      let dir = '';
      if (st > this.lastScrollTop) {
        dir = "down";
      } else {
        dir = "up";
      }
      this.lastScrollTop = st;
    }
   }

  ngOnInit(): void {
    this.header = this.header.el;
  }

  @HostListener('ionScroll', ['$event']) onContentScroll($event) {
    const scrollTop = $event.detail.scrollTop;
    /*console.log(scrollTop)*/

    if (scrollTop > this.lastScrollTop && !this.home.buttonsVisible){
      this.newOpacity = 0;
      /*console.log("Scrolling down")*/
      this.domCtrl.write(() => {
        this.renderer.setStyle(this.header, 'top', 0);
        this.renderer.setStyle(this.header, 'opacity', this.newOpacity);
      })  
    } else if (this.home.buttonsVisible){
      this.newOpacity = 1;
      this.domCtrl.write(() => {
        this.renderer.setStyle(this.header, 'top', 0);
        this.renderer.setStyle(this.header, 'opacity', this.newOpacity);
      })
    }else {
      this.newOpacity = this.newOpacity + 0.1;
      /*console.log("Scrolling up")*/
      this.domCtrl.write(() => {
        this.renderer.setStyle(this.header, 'top', 0);
        this.renderer.setStyle(this.header, 'opacity', this.newOpacity);
      })  
    }
    this.lastScrollTop = scrollTop
  }

}
