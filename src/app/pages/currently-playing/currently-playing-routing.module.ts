import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CurrentlyPlayingPage } from './currently-playing.page';

const routes: Routes = [
  {
    path: '',
    component: CurrentlyPlayingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CurrentlyPlayingPageRoutingModule {}
