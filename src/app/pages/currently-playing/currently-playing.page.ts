import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VideogamescrudService } from 'src/app/services/videogamescrud.service';

@Component({
  selector: 'app-currently-playing',
  templateUrl: './currently-playing.page.html',
  styleUrls: ['./currently-playing.page.scss'],
})
export class CurrentlyPlayingPage implements OnInit {
  videogamesList: Array<any>;

  constructor(private gamesCrud: VideogamescrudService,private router:Router) { }

  ngOnInit() {
    this.getListGame();
  }

  getListGame() {
    this.gamesCrud.getVideogamesFromFirebase('playing').subscribe((res) =>{
      this.videogamesList = res;
      console.log(this.videogamesList)
    });
  }

  removeVideogame(name: string) {
    if(name !== null) {
      this.gamesCrud.removeSingleVideogame('playing', name);
    }
  }

  openGameDetails(id: string):void{
    this.router.navigate(['detail',id]);
  }

}
