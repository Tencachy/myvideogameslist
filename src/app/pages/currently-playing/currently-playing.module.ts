import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CurrentlyPlayingPageRoutingModule } from './currently-playing-routing.module';

import { CurrentlyPlayingPage } from './currently-playing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CurrentlyPlayingPageRoutingModule
  ],
  declarations: [CurrentlyPlayingPage]
})
export class CurrentlyPlayingPageModule {}
