import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TopGamesPageRoutingModule } from './top-games-routing.module';

import { TopGamesPage } from './top-games.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TopGamesPageRoutingModule
  ],
  declarations: [TopGamesPage]
})
export class TopGamesPageModule {}
