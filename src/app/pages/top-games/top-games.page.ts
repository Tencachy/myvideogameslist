import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { APIResponse, Game } from 'src/app/models';
import { GamesService } from 'src/app/services/games.service';

@Component({
  selector: 'app-top-games',
  templateUrl: './top-games.page.html',
  styleUrls: ['./top-games.page.scss'],
})
export class TopGamesPage implements OnInit {

  public games:Array<Game>;
  
  currentVideogame: Game;

  constructor(private router:Router, private http: GamesService, private activatedRoute:ActivatedRoute,) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params:Params)=>{
        this.searchGames('-rating');
    })
  }

  searchGames(sort:string):void{
    this.http.getGameList(sort).subscribe(async (gameList:APIResponse<Game>)=>{

      this.games=gameList.results;

      
    });
  }

  getGameFromList(id: number): Game {
    return this.games.find(x => x.id === id);
  }


  openGameDetails(id: number):void{
    this.router.navigate(['detail',id]);
  }

}
