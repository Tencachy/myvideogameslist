import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './../../services/authentication.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.page.html',
  styleUrls: ['./forget-password.page.scss'],
})
export class ForgetPasswordPage implements OnInit {
  email: string;

  constructor(
    private authentication: AuthenticationService,
  ) { }

  ngOnInit() {
  }

  resetPassword() {
    this.authentication.resetPassword(this.email);
  }

}
