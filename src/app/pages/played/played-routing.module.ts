import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlayedPage } from './played.page';

const routes: Routes = [
  {
    path: '',
    component: PlayedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlayedPageRoutingModule {}
