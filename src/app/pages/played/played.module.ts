import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlayedPageRoutingModule } from './played-routing.module';

import { PlayedPage } from './played.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlayedPageRoutingModule
  ],
  declarations: [PlayedPage]
})
export class PlayedPageModule {}
