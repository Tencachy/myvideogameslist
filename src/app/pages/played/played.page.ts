import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonList } from '@ionic/angular';
import { Game } from 'src/app/models';
import { VideogamescrudService } from 'src/app/services/videogamescrud.service';

@Component({
  selector: 'app-played',
  templateUrl: './played.page.html',
  styleUrls: ['./played.page.scss'],
})
export class PlayedPage implements OnInit {
  videogamesList: Array<any>;
  @ViewChild (IonList)ionList:IonList;

  constructor(
    private router:Router,
    private gamesCrud: VideogamescrudService,
  ) { }

  ngOnInit() {
    this.getListGame();
  }

  getListGame() {
    this.gamesCrud.getVideogamesFromFirebase('played').subscribe((res) =>{
      this.videogamesList = res;
      console.log(this.videogamesList)
    });
  }

  removeVideogame(name: string) {
    if(name !== null) {
      this.gamesCrud.removeSingleVideogame('played', name);
    }
  }

  openGameDetails(id: string):void{
    this.router.navigate(['detail',id]);
  }

  
}
