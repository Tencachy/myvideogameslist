import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input, ViewChild, Renderer2 } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ActionSheetController, IonList, IonToolbar } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { IonContent, DomController } from '@ionic/angular';
import { APIResponse, Game } from 'src/app/models';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { GamesService } from 'src/app/services/games.service';
import { LocalstorageService } from 'src/app/services/localstorage.service';
import { VideogamescrudService } from 'src/app/services/videogamescrud.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  
  currentVideogame: Game;
  public sort:string;
  public games:Array<Game>;
  toggled:boolean;
  videogamesListWantToPlay: Array<any>;
  videogamesListPlaying: Array<any>;
  videogamesListPlayed: Array<any>;
  date:Date;
  currentDate:string;
  public buttonsVisible: boolean;

  @ViewChild (IonList)ionList:IonList;
  @ViewChild(IonContent) content: IonContent;

  constructor(
    private auth: AuthenticationService, 
    private videogameService: VideogamescrudService, 
    private http: GamesService,
    private activatedRoute:ActivatedRoute,
    private actionSheetCtrl:ActionSheetController,
    private navCtrl: NavController,
    private localStrg: LocalstorageService,
    private router:Router,
    private domCtrl: DomController, 
    private renderer: Renderer2,
    public datePipe:DatePipe) {
    this.toggled=false;
    this.buttonsVisible = false;
   }

  ngOnInit():void {
    this.auth.getCurrentUserId();
    this.activatedRoute.params.subscribe((params:Params)=>{
      if(params['game-search']){
        this.searchGames('-metacritic',params['game-search']);
      }else{
        this.searchGames('-metacritic');
      }
    })
    this.auth.getUser();
  }

  toggleSearch() {
    this.buttonsVisible = this.buttonsVisible ? false : true;    
    this.toggled = this.toggled ? false : true;
  }

  scrollTop() {
    this.content.scrollToTop(500)
  }

  storeVideogame(id: number, list:string) {
    this.currentVideogame = this.getGameFromList(id);
    this.videogameService.pushVideogame(this.currentVideogame, list);
    this.ionList.closeSlidingItems();
  }

  getVideogamesFromLists() {
    this.videogameService.getVideogamesFromFirebase('wantToPlay').subscribe((res) => {
      this.videogamesListWantToPlay = res;

      this.compareVideogameName(this.videogamesListWantToPlay, 'wantToPlay')
    })

    this.videogameService.getVideogamesFromFirebase('playing').subscribe((res) => {
      this.videogamesListPlaying = res;

      this.compareVideogameName(this.videogamesListPlaying, 'playing')
    })

    this.videogameService.getVideogamesFromFirebase('played').subscribe((res) => {
      this.videogamesListPlayed = res;

      this.compareVideogameName(this.videogamesListPlayed, 'played')
    })
  }

  compareVideogameName(list: Array<any>, path: string) {
    const applist = this.games;
    const firebaselist = list;

    for (var i=0; i<this.games.length; i++){
      for (var j=0; j<list.length; j++){
        if (applist[i].name === firebaselist[j].videogame.name) {
          applist[i].state = path;
        }
        applist[i].state === 'wantToPlay' ? applist[i].optionColorWantToPlay = 'tertiary' : applist[i].optionColorWantToPlay = 'light'
        applist[i].state === 'playing' ? applist[i].optionColorPlaying = 'tertiary' : applist[i].optionColorPlaying = 'light'
        applist[i].state === 'played' ? applist[i].optionColorPlayed = 'tertiary' : applist[i].optionColorPlayed = 'light'
      }
    }
  }

  getGameFromList(id: number): Game {
    return this.games.find(x => x.id === id);
  }

  getActualDate(){
    this.date=new Date();
    this.currentDate =this.datePipe.transform(this.date, 'yyyy-MM-dd');
  }


  searchGames(sort:string,search?:string):void{
    this.http.getGameList(sort,search).subscribe(async (gameList:APIResponse<Game>)=>{

      this.games=gameList.results;

      this.getVideogamesFromLists();
    });
  }

  openGameDetails(id: number):void{
    this.router.navigate(['detail',id]);
  }

  async platformSearch() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Platform',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'PS4',
        handler: () => {
          this.http.getGamesByPlatform('-metacritic',18).subscribe((gamelist:APIResponse<Game>)=>{this.games=gamelist.results});
        }
      }, {
        text: 'PS5',
        handler: () => {
          this.http.getGamesByPlatform('-metacritic',187).subscribe((gamelist:APIResponse<Game>)=>{this.games=gamelist.results});
        }
      }, {
        text: 'Xbox One',
        handler: () => {
          this.http.getGamesByPlatform('-metacritic',1).subscribe((gamelist:APIResponse<Game>)=>{this.games=gamelist.results});
        }
      }, {
        text: 'Xbox Series X',
        handler: () => {
          this.http.getGamesByPlatform('-metacritic',186).subscribe((gamelist:APIResponse<Game>)=>{this.games=gamelist.results});
        }
      }, {
        text: 'Nintendo Switch',
        handler: () => {
          this.http.getGamesByPlatform('-metacritic',7).subscribe((gamelist:APIResponse<Game>)=>{this.games=gamelist.results});
        }
      },{
        text: 'PC',
        handler: () => {
          this.http.getGamesByPlatform('-metacritic',4).subscribe((gamelist:APIResponse<Game>)=>{this.games=gamelist.results});
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async genreSearch() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Genre',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Action',
        handler: () => {
        
          this.http.getGamesByGenre('-metacritic','genres','action').subscribe((gamelist:APIResponse<Game>)=>{this.games=gamelist.results});
        }
      }, {
        text: 'Adventure',
        handler: () => {
       
          this.http.getGamesByGenre('-metacritic','genres','adventure').subscribe((gamelist:APIResponse<Game>)=>{this.games=gamelist.results});
        }
      }, {
        text: 'RPG',
        handler: () => {
         
          this.http.getGamesByGenre('-metacritic','genres','role-playing-games-rpg').subscribe((gamelist:APIResponse<Game>)=>{this.games=gamelist.results});
        }
      }, {
        text: 'Strategy',
        handler: () => {
        
          this.http.getGamesByGenre('-metacritic','genres','strategy').subscribe((gamelist:APIResponse<Game>)=>{this.games=gamelist.results});
        }
      }, {
        text: 'Shooter',
        handler: () => {
      
          this.http.getGamesByGenre('-metacritic','genres','shooter').subscribe((gamelist:APIResponse<Game>)=>{this.games=gamelist.results});
        }
      },{
        text: 'Casual',
        handler: () => {
      
          this.http.getGamesByGenre('-metacritic','genres','casual').subscribe((gamelist:APIResponse<Game>)=>{this.games=gamelist.results});
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async orderBy() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Order By',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Name',
        handler: () => {
          this.searchGames('name');
        }
      }, {
        text: 'Release Date',
        handler: () => {
          this.getActualDate();
          console.log('2021-01-01,'+this.currentDate)
          this.http.getGamesByGenre('-released','dates','2021-01-01,'+this.currentDate).subscribe((gamelist:APIResponse<Game>)=>{this.games=gamelist.results});
        }
      }, {
        text: 'Rating',
        handler: () => {
          this.searchGames('-rating');
        }
      }, {
        text: 'Metacritic',
        handler: () => {
          this.searchGames('-metacritic');
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }


}
