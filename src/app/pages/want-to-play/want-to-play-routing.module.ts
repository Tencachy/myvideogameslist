import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WantToPlayPage } from './want-to-play.page';

const routes: Routes = [
  {
    path: '',
    component: WantToPlayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WantToPlayPageRoutingModule {}
