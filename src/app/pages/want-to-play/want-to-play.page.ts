import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VideogamescrudService } from 'src/app/services/videogamescrud.service';

@Component({
  selector: 'app-want-to-play',
  templateUrl: './want-to-play.page.html',
  styleUrls: ['./want-to-play.page.scss'],
})
export class WantToPlayPage implements OnInit {
  videogamesList: Array<any>;

  constructor(private gamesCrud: VideogamescrudService,private router:Router,) { }

  ngOnInit() {
    this.getListGame();
  }

  getListGame() {
    this.gamesCrud.getVideogamesFromFirebase('wantToPlay').subscribe((res) =>{
      this.videogamesList = res;
      console.log(this.videogamesList)
    });
  }

  removeVideogame(name: string) {
    if(name !== null) {
      this.gamesCrud.removeSingleVideogame('wantToPlay', name);
    }
  }

  openGameDetails(id: string):void{
    this.router.navigate(['detail',id]);
  }


}
