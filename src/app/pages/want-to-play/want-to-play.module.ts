import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WantToPlayPageRoutingModule } from './want-to-play-routing.module';

import { WantToPlayPage } from './want-to-play.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WantToPlayPageRoutingModule
  ],
  declarations: [WantToPlayPage]
})
export class WantToPlayPageModule {}
