import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  dark_theme: boolean;

  constructor(
    private appcomponent: AppComponent
  ) { }

  ngOnInit() {
  }

  toggleTheme(event){
    if(event.detail.checked){
      this.appcomponent.dark_theme = true;
      //this.dark_theme = true;
      document.body.setAttribute('color-theme', 'dark')
    }else{
      this.appcomponent.dark_theme = false;
      //this.dark_theme = false;
      document.body.setAttribute('color-theme', 'light')
    }
  }

}
