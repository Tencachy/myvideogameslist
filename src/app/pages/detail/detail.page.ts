import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Params } from '@angular/router';
import { IonSlides, IonContent } from '@ionic/angular';
import { Observable, Subscription } from 'rxjs';
import { APIResponse, Game, Screenshots, Trailer } from 'src/app/models';
import { GamesService } from 'src/app/services/games.service';
import { VideogamescrudService } from 'src/app/services/videogamescrud.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit,OnDestroy{

  gameId: string;
  game:Game;
  trailers:Array<Trailer>
  screen:Array<Screenshots>;
  routeSub:Subscription;
  gameSub:Subscription;
  screenSub:Subscription;
  videogamesListWantToPlay: Array<any>;
  videogamesListPlaying: Array<any>;
  videogamesListPlayed: Array<any>;

  

  @ViewChild('slides') slider: IonSlides;
  @ViewChild(IonContent) content: IonContent;

  segment = 0;

  constructor(
    private dom:DomSanitizer,
    private activatedRoute: ActivatedRoute,
    private http: GamesService,
    private videogameService: VideogamescrudService,) { }

  ngOnInit():void {
    this.gameId = this.activatedRoute.snapshot.paramMap.get('id')
  this.http.getGameDetails( this.gameId ).subscribe(
    res=>{
      this.game = res;
      this.getVideogamesFromLists();
    }
  )
  this.getGameScreenshots(this.gameId);
  }


  ngOnDestroy():void{
    if(this.gameSub){
      this.gameSub.unsubscribe();
    }

    if(this.routeSub){
      this.routeSub.unsubscribe();
    }
  }

  getGameDetails(id:string):void{
    this.gameSub=this.http
    .getGameDetails(id)
    .subscribe((gameResp:Game)=>{
      this.game=gameResp
     


      setTimeout(()=>{
      
      },1000);
    });
    
  }

  getGameScreenshots(id:string):void{
    this.http.getGameScreenshots(id).subscribe((gameList:APIResponse<Screenshots>)=>{
      this.screen=gameList.results;
    });
  }

  getGameTrailers(id:string):void{
    this.http.getGameTrailers(id).subscribe((gameList:APIResponse<Trailer>)=>{
      this.trailers=gameList.results;
    })
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
    this.goTop()
  }

  goTop() {
    this.content.scrollToTop(500)
  }
  getVideogamesFromLists() {
    this.videogameService.getVideogamesFromFirebase('wantToPlay').subscribe((res) => {
      this.videogamesListWantToPlay = res;

      this.compareVideogameName(this.videogamesListWantToPlay, 'wantToPlay')
    })

    this.videogameService.getVideogamesFromFirebase('playing').subscribe((res) => {
      this.videogamesListPlaying = res;

      this.compareVideogameName(this.videogamesListPlaying, 'playing')
    })

    this.videogameService.getVideogamesFromFirebase('played').subscribe((res) => {
      this.videogamesListPlayed = res;

      this.compareVideogameName(this.videogamesListPlayed, 'played')
    })

  }

  compareVideogameName(list: Array<any>, path: string) {
    const firebaselist = list;
      for (var i=0; i<list.length; i++){
        if (this.game.name === firebaselist[i].videogame.name) {
          this.game.state = path;
          
          this.game.state === 'wantToPlay' ? this.game.optionColorWantToPlay = 'tertiary' : this.game.optionColorWantToPlay = 'secondary'
          this.game.state === 'playing' ? this.game.optionColorPlaying = 'tertiary' : this.game.optionColorPlaying = 'secondary'
          this.game.state === 'played' ? this.game.optionColorPlayed = 'tertiary' : this.game.optionColorPlayed = 'secondary'
        }else{
          this.game.state === 'wantToPlay' ? this.game.optionColorWantToPlay = 'tertiary' : this.game.optionColorWantToPlay = 'secondary'
          this.game.state === 'playing' ? this.game.optionColorPlaying = 'tertiary' : this.game.optionColorPlaying = 'secondary'
          this.game.state === 'played' ? this.game.optionColorPlayed = 'tertiary' : this.game.optionColorPlayed = 'secondary'
        }
      }
    
  }

  storeVideogame(list:string) {
    this.videogameService.pushVideogame(this.game, list);
    this.getVideogamesFromLists()

  }

 
}
