import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user: any = {};

  constructor(
    private router: Router, 
    private authentication: AuthenticationService,
  ) { }

  async ngOnInit() {}

  login() {
    this.authentication.login(this.user);
  }

  goRegister() {
    this.router.navigateByUrl('/register');
  }

  goForgetPassword() {
    this.router.navigateByUrl('/forget-password');
  }

  goHome() {
    this.router.navigateByUrl('/home');
  }

}
