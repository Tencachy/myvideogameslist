import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './../../services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  user: any = {};

  constructor(
    private router: Router, 
    private authentication: AuthenticationService,
  ) { }

  ngOnInit() {
  }

  register() {
    this.authentication.register(this.user);
  }

  goLogin() {
    this.router.navigateByUrl('/login')
  }

}
