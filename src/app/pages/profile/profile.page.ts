import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { LocalstorageService } from 'src/app/services/localstorage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  username: string
  public profileimage = '../assets/profile_light.png'

  constructor(
    private localStrg: LocalstorageService, 
    private authentication: AuthenticationService,
    private router: Router, 
    private appcomponent: AppComponent
  ) { 
    this.getUsername();
  }

  ngOnInit() {
    this.changeProfileImage();
  }

  getUsername() {
    this.username = this.authentication.user.username;
  }

  goWantToPlay() {
    this.router.navigateByUrl('/want-to-play');
  }

  goPlaying() {
    this.router.navigateByUrl('/currently-playing')
  }

  goPlayed() {
    this.router.navigateByUrl('/played')
  }

  changeProfileImage() {
    if (this.appcomponent.dark_theme) {
      this.profileimage = '../assets/profile_dark.png'
    } else {
      this.profileimage = '../assets/profile_light.png'
    }
  }

}
