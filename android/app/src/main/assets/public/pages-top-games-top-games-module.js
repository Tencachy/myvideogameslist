(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-top-games-top-games-module"],{

/***/ "I4jR":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/top-games/top-games.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title color=\"primary\">Top games</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button color=\"primary\" menu=\"main-menu\"></ion-menu-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n      <div>  \r\n      <ion-card   color=\"primary\"   *ngFor=\"let game of games\" >\r\n        <ion-card-content class=\"ion-text-center\" (click)=\"openGameDetails(game.id)\">\r\n        <img *ngIf=\"game.background_image\" src=\"{{game.background_image}}\" class=\"itemImage\" >\r\n        <img *ngIf=\"!game.background_image\" src=\"./assets/noimage.jpg\" class=\"itemImage\" >\r\n        <ion-card-title>{{game.name}}</ion-card-title>\r\n        <ion-card-subtitle>{{game.rating}} / 5</ion-card-subtitle>\r\n    </ion-card-content>\r\n    </ion-card> </div>\r\n   \r\n\r\n\r\n  \r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "Ztx7":
/*!*****************************************************!*\
  !*** ./src/app/pages/top-games/top-games.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".listItem {\n  margin: 10px;\n}\n\n.itemImage {\n  max-height: 144px;\n  max-width: 256px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFx0b3AtZ2FtZXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtBQUNKOztBQUdDO0VBQ0csaUJBQUE7RUFDQSxnQkFBQTtBQUFKIiwiZmlsZSI6InRvcC1nYW1lcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubGlzdEl0ZW17XHJcbiAgICBtYXJnaW46IDEwcHg7XHJcbiAgXHJcbiB9XHJcbiBcclxuIC5pdGVtSW1hZ2V7XHJcbiAgICBtYXgtaGVpZ2h0OiAxNDRweDtcclxuICAgIG1heC13aWR0aDogMjU2cHg7IFxyXG4gfVxyXG4gXHJcblxyXG4gXHJcbiBcclxuIFxyXG4gIl19 */");

/***/ }),

/***/ "mxlt":
/*!*****************************************************!*\
  !*** ./src/app/pages/top-games/top-games.module.ts ***!
  \*****************************************************/
/*! exports provided: TopGamesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopGamesPageModule", function() { return TopGamesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _top_games_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./top-games-routing.module */ "qYrn");
/* harmony import */ var _top_games_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./top-games.page */ "y5VC");







let TopGamesPageModule = class TopGamesPageModule {
};
TopGamesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _top_games_routing_module__WEBPACK_IMPORTED_MODULE_5__["TopGamesPageRoutingModule"]
        ],
        declarations: [_top_games_page__WEBPACK_IMPORTED_MODULE_6__["TopGamesPage"]]
    })
], TopGamesPageModule);



/***/ }),

/***/ "qYrn":
/*!*************************************************************!*\
  !*** ./src/app/pages/top-games/top-games-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: TopGamesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopGamesPageRoutingModule", function() { return TopGamesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _top_games_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./top-games.page */ "y5VC");




const routes = [
    {
        path: '',
        component: _top_games_page__WEBPACK_IMPORTED_MODULE_3__["TopGamesPage"]
    }
];
let TopGamesPageRoutingModule = class TopGamesPageRoutingModule {
};
TopGamesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TopGamesPageRoutingModule);



/***/ }),

/***/ "y5VC":
/*!***************************************************!*\
  !*** ./src/app/pages/top-games/top-games.page.ts ***!
  \***************************************************/
/*! exports provided: TopGamesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopGamesPage", function() { return TopGamesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_top_games_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./top-games.page.html */ "I4jR");
/* harmony import */ var _top_games_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./top-games.page.scss */ "Ztx7");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_games_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/games.service */ "wZ2L");






let TopGamesPage = class TopGamesPage {
    constructor(router, http, activatedRoute) {
        this.router = router;
        this.http = http;
        this.activatedRoute = activatedRoute;
    }
    ngOnInit() {
        this.activatedRoute.params.subscribe((params) => {
            this.searchGames('-rating');
        });
    }
    searchGames(sort) {
        this.http.getGameList(sort).subscribe((gameList) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.games = gameList.results;
        }));
    }
    getGameFromList(id) {
        return this.games.find(x => x.id === id);
    }
    openGameDetails(id) {
        this.router.navigate(['detail', id]);
    }
};
TopGamesPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_games_service__WEBPACK_IMPORTED_MODULE_5__["GamesService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
];
TopGamesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-top-games',
        template: _raw_loader_top_games_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_top_games_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TopGamesPage);



/***/ })

}]);
//# sourceMappingURL=pages-top-games-top-games-module.js.map