(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "/rnz":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".listItem {\n  margin: 10px;\n  border-radius: 12px 12px 12px 12px;\n}\n\n.headerButtons {\n  position: sticky;\n}\n\n.arrow-up {\n  border-radius: 38px 38px 38px 38px;\n  margin: 4px;\n  padding: 4px;\n  background-color: var(--ion-color-primary);\n}\n\n.itemThumbail {\n  height: 80px;\n  width: 120px;\n  margin: 8px;\n}\n\n.itemImage {\n  object-fit: fill;\n}\n\n.itemLabelText {\n  vertical-align: text-top;\n  font-size: 15px;\n  font-weight: bolder;\n}\n\nion-icon {\n  display: inline-block;\n  font-size: 30px;\n  vertical-align: middle;\n}\n\n.iconText {\n  font-size: 12px;\n  font-weight: bolder;\n  color: azure;\n  display: inline-block;\n  vertical-align: middle;\n}\n\n.filterButtons {\n  padding: 12px;\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  position: sticky;\n  top: 0px;\n  z-index: 1;\n  background-color: var(--ion-color-light);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNHLFlBQUE7RUFDQyxrQ0FBQTtBQUNKOztBQUVBO0VBQ0ksZ0JBQUE7QUFDSjs7QUFFQTtFQUNJLGtDQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSwwQ0FBQTtBQUNKOztBQUdBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBQUo7O0FBR0E7RUFDSSxnQkFBQTtBQUFKOztBQUdBO0VBQ0ksd0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUFBSjs7QUFJQTtFQUNJLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0FBREo7O0FBSUE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7RUFDQSxzQkFBQTtBQURKOztBQU1BO0VBRUksYUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSx3Q0FBQTtBQUpKIiwiZmlsZSI6ImhvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxpc3RJdGVte1xyXG4gICBtYXJnaW46IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4IDEycHggMTJweCAxMnB4O1xyXG59XHJcblxyXG4uaGVhZGVyQnV0dG9uc3tcclxuICAgIHBvc2l0aW9uOiBzdGlja3k7XHJcbn1cclxuXHJcbi5hcnJvdy11cHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDM4cHggMzhweCAzOHB4IDM4cHg7XHJcbiAgICBtYXJnaW46NHB4O1xyXG4gICAgcGFkZGluZzogNHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgXHJcbn1cclxuXHJcbi5pdGVtVGh1bWJhaWx7XHJcbiAgICBoZWlnaHQ6IDgwcHg7XHJcbiAgICB3aWR0aDogMTIwcHg7XHJcbiAgICBtYXJnaW46IDhweDsgXHJcbn1cclxuXHJcbi5pdGVtSW1hZ2V7XHJcbiAgICBvYmplY3QtZml0OmZpbGw7IFxyXG59XHJcblxyXG4uaXRlbUxhYmVsVGV4dHtcclxuICAgIHZlcnRpY2FsLWFsaWduOiB0ZXh0LXRvcDtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbn1cclxuXHJcblxyXG5pb24taWNvbiB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gIH1cclxuXHJcbi5pY29uVGV4dHtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbiAgICBjb2xvcjogYXp1cmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcblxyXG5cclxuXHJcbi5maWx0ZXJCdXR0b25ze1xyXG4gIFxyXG4gICAgcGFkZGluZzogMTJweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHBvc2l0aW9uOiBzdGlja3k7XHJcbiAgICB0b3A6MHB4O1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcblxyXG4gICAgXHJcbn1cclxuXHJcblxyXG5cclxuIl19 */");

/***/ }),

/***/ "99Un":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-routing.module */ "9oos");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "hsj+");
/* harmony import */ var src_app_directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/directives/shared-directives.module */ "Fgaq");








let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomePageRoutingModule"],
            src_app_directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_7__["SharedDirectivesModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "9oos":
/*!***************************************************!*\
  !*** ./src/app/pages/home/home-routing.module.ts ***!
  \***************************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "hsj+");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomePageRoutingModule);



/***/ }),

/***/ "Fgaq":
/*!********************************************************!*\
  !*** ./src/app/directives/shared-directives.module.ts ***!
  \********************************************************/
/*! exports provided: SharedDirectivesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedDirectivesModule", function() { return SharedDirectivesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _hide_header_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./hide-header.directive */ "Ut7A");




let SharedDirectivesModule = class SharedDirectivesModule {
};
SharedDirectivesModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _hide_header_directive__WEBPACK_IMPORTED_MODULE_3__["HideHeaderDirective"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
        ],
        exports: [
            _hide_header_directive__WEBPACK_IMPORTED_MODULE_3__["HideHeaderDirective"]
        ]
    })
], SharedDirectivesModule);



/***/ }),

/***/ "Ut7A":
/*!*****************************************************!*\
  !*** ./src/app/directives/hide-header.directive.ts ***!
  \*****************************************************/
/*! exports provided: HideHeaderDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HideHeaderDirective", function() { return HideHeaderDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _pages_home_home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../pages/home/home.page */ "hsj+");




let HideHeaderDirective = class HideHeaderDirective {
    constructor(renderer, domCtrl, home, lc) {
        this.renderer = renderer;
        this.domCtrl = domCtrl;
        this.home = home;
        this.lastScrollTop = 0;
        this.newOpacity = null;
        window.onscroll = () => {
            let st = window.pageYOffset;
            let dir = '';
            if (st > this.lastScrollTop) {
                dir = "down";
            }
            else {
                dir = "up";
            }
            this.lastScrollTop = st;
        };
    }
    ngOnInit() {
        this.header = this.header.el;
    }
    onContentScroll($event) {
        const scrollTop = $event.detail.scrollTop;
        /*console.log(scrollTop)*/
        if (scrollTop > this.lastScrollTop && !this.home.buttonsVisible) {
            this.newOpacity = 0;
            /*console.log("Scrolling down")*/
            this.domCtrl.write(() => {
                this.renderer.setStyle(this.header, 'top', 0);
                this.renderer.setStyle(this.header, 'opacity', this.newOpacity);
            });
        }
        else if (this.home.buttonsVisible) {
            this.newOpacity = 1;
            this.domCtrl.write(() => {
                this.renderer.setStyle(this.header, 'top', 0);
                this.renderer.setStyle(this.header, 'opacity', this.newOpacity);
            });
        }
        else {
            this.newOpacity = this.newOpacity + 0.1;
            /*console.log("Scrolling up")*/
            this.domCtrl.write(() => {
                this.renderer.setStyle(this.header, 'top', 0);
                this.renderer.setStyle(this.header, 'opacity', this.newOpacity);
            });
        }
        this.lastScrollTop = scrollTop;
    }
};
HideHeaderDirective.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["DomController"] },
    { type: _pages_home_home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
];
HideHeaderDirective.propDecorators = {
    header: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"], args: ['appHideHeader',] }],
    onContentScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"], args: ['ionScroll', ['$event'],] }]
};
HideHeaderDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: '[appHideHeader]'
    })
], HideHeaderDirective);



/***/ }),

/***/ "eUf4":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title *ngIf=\"!toggled\" color=\"primary\">Home</ion-title>\r\n    <ion-title *ngIf=\"toggled\"color=\"primary\">Search</ion-title>\r\n    <ion-buttons slot=\"start\" >\r\n      <ion-menu-button color=\"primary\" menu=\"main-menu\"></ion-menu-button>\r\n    </ion-buttons>\r\n\r\n\r\n  </ion-toolbar>\r\n\r\n</ion-header>\r\n\r\n<ion-content scrollEvents=\"true\" [appHideHeader]=\"header\">\r\n\r\n  <ion-toolbar #header class=\"headerButtons\">\r\n    <div class=\"filterButtons\">\r\n      <ion-searchbar *ngIf=\"toggled\" animated (search)=\"searchGames('-metacritic',$event.target.value)\" (ionCancel)=\"toggleSearch()\" color=\"secondary\"></ion-searchbar>\r\n      <ion-button *ngIf=\"!toggled\" color=\"primary\"  (click)=\"genreSearch()\">\r\n        Genre\r\n      </ion-button>\r\n      <ion-button *ngIf=\"!toggled\" color=\"primary\" (click)=\"platformSearch()\">\r\n        Plataform\r\n      </ion-button>\r\n      <ion-button *ngIf=\"!toggled\" color=\"primary\" (click)=\"orderBy()\">\r\n        Order By\r\n      </ion-button>\r\n      <ion-icon color=\"secondary\" name=\"arrow-up\" slot=\"end\" (click)=\"scrollTop()\" class=\"arrow-up\"></ion-icon>\r\n    </div>\r\n  </ion-toolbar>\r\n  \r\n  \r\n  <ion-list>\r\n\r\n    <ion-item-sliding *ngFor=\"let game of games\" class=\"listItem\" (ionDrag)=\"getVideogamesFromLists()\"> \r\n      <ion-item-options side=\"end\">\r\n        <ion-item-option (click)=\"storeVideogame(game.id,'wantToPlay')\" [color]=\"game.optionColorWantToPlay\">\r\n          <ion-icon slot=\"icon-only\" name=\"locate-outline\"></ion-icon>\r\n        </ion-item-option >\r\n        <ion-item-option (click)=\"storeVideogame(game.id,'playing')\" [color]=\"game.optionColorPlaying\">\r\n          <ion-icon slot=\"icon-only\" name=\"game-controller-outline\"></ion-icon>\r\n        </ion-item-option >\r\n        <ion-item-option  (click)=\"storeVideogame(game.id,'played')\" [color]=\"game.optionColorPlayed\">\r\n          <ion-icon slot=\"icon-only\" name=\"lock-closed-outline\"></ion-icon>\r\n        </ion-item-option >\r\n      </ion-item-options>\r\n      <ion-item color=\"primary\" (click)=\"openGameDetails(game.id)\">\r\n      <ion-thumbnail slot=\"start\"  class=\"itemThumbail\" (click)=\"openGameDetails(game.id)\">\r\n        <img *ngIf=\"game.background_image\" src=\"{{game.background_image}}\" class=\"itemImage\" >\r\n        <img *ngIf=\"!game.background_image\" src=\"./assets/noimage.jpg\" class=\"itemImage\" >\r\n      </ion-thumbnail>\r\n      <ion-label >\r\n        <p class=\"itemLabelText\">{{game.name}}</p>\r\n        <p class=\"itemLabelText\" >({{game.released}})</p>\r\n        <p class=\"itemLabelText\" *ngIf=\"game.rating>0\" >{{game.rating}}  </p>\r\n        <p class=\"itemLabelText\" *ngIf=\"1>game.rating\" >---</p>\r\n       <span class=\"itemLabelText\" *ngFor=\"let pf of game.parent_platforms;let last=last\"  >{{pf.platform.name}} <span *ngIf=\"!last\">-\r\n       </span></span>\r\n      </ion-label>\r\n    </ion-item>\r\n   </ion-item-sliding>\r\n\r\n  </ion-list>\r\n\r\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\" >\r\n    <ion-fab-button class=\"fabButton\" color=\"secondary\">\r\n      <ion-icon *ngIf=\"!toggled\" (click)=\"toggleSearch()\" name=\"search-outline\"></ion-icon>\r\n      <ion-icon *ngIf=\"toggled\" (click)=\"toggleSearch()\" name=\"close-outline\"></ion-icon>\r\n    </ion-fab-button>\r\n  </ion-fab>\r\n</ion-content>\r\n");

/***/ }),

/***/ "hsj+":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./home.page.html */ "eUf4");
/* harmony import */ var _home_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home.page.scss */ "/rnz");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/authentication.service */ "ej43");
/* harmony import */ var src_app_services_games_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/games.service */ "wZ2L");
/* harmony import */ var src_app_services_localstorage_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/localstorage.service */ "FAUO");
/* harmony import */ var src_app_services_videogamescrud_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/videogamescrud.service */ "RfOB");













let HomePage = class HomePage {
    constructor(auth, videogameService, http, activatedRoute, actionSheetCtrl, navCtrl, localStrg, router, domCtrl, renderer, datePipe) {
        this.auth = auth;
        this.videogameService = videogameService;
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.actionSheetCtrl = actionSheetCtrl;
        this.navCtrl = navCtrl;
        this.localStrg = localStrg;
        this.router = router;
        this.domCtrl = domCtrl;
        this.renderer = renderer;
        this.datePipe = datePipe;
        this.toggled = false;
        this.buttonsVisible = false;
    }
    ngOnInit() {
        this.auth.getCurrentUserId();
        this.activatedRoute.params.subscribe((params) => {
            if (params['game-search']) {
                this.searchGames('-metacritic', params['game-search']);
            }
            else {
                this.searchGames('-metacritic');
            }
        });
        this.auth.getUser();
    }
    toggleSearch() {
        this.buttonsVisible = this.buttonsVisible ? false : true;
        this.toggled = this.toggled ? false : true;
    }
    scrollTop() {
        this.content.scrollToTop(500);
    }
    storeVideogame(id, list) {
        this.currentVideogame = this.getGameFromList(id);
        this.videogameService.pushVideogame(this.currentVideogame, list);
        this.ionList.closeSlidingItems();
    }
    getVideogamesFromLists() {
        this.videogameService.getVideogamesFromFirebase('wantToPlay').subscribe((res) => {
            this.videogamesListWantToPlay = res;
            this.compareVideogameName(this.videogamesListWantToPlay, 'wantToPlay');
        });
        this.videogameService.getVideogamesFromFirebase('playing').subscribe((res) => {
            this.videogamesListPlaying = res;
            this.compareVideogameName(this.videogamesListPlaying, 'playing');
        });
        this.videogameService.getVideogamesFromFirebase('played').subscribe((res) => {
            this.videogamesListPlayed = res;
            this.compareVideogameName(this.videogamesListPlayed, 'played');
        });
    }
    compareVideogameName(list, path) {
        const applist = this.games;
        const firebaselist = list;
        for (var i = 0; i < this.games.length; i++) {
            for (var j = 0; j < list.length; j++) {
                if (applist[i].name === firebaselist[j].videogame.name) {
                    applist[i].state = path;
                }
                applist[i].state === 'wantToPlay' ? applist[i].optionColorWantToPlay = 'tertiary' : applist[i].optionColorWantToPlay = 'light';
                applist[i].state === 'playing' ? applist[i].optionColorPlaying = 'tertiary' : applist[i].optionColorPlaying = 'light';
                applist[i].state === 'played' ? applist[i].optionColorPlayed = 'tertiary' : applist[i].optionColorPlayed = 'light';
            }
        }
    }
    getGameFromList(id) {
        return this.games.find(x => x.id === id);
    }
    getActualDate() {
        this.date = new Date();
        this.currentDate = this.datePipe.transform(this.date, 'yyyy-MM-dd');
    }
    searchGames(sort, search) {
        this.http.getGameList(sort, search).subscribe((gameList) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.games = gameList.results;
            this.getVideogamesFromLists();
        }));
    }
    openGameDetails(id) {
        this.router.navigate(['detail', id]);
    }
    platformSearch() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetCtrl.create({
                header: 'Select Platform',
                cssClass: 'my-custom-class',
                buttons: [{
                        text: 'PS4',
                        handler: () => {
                            this.http.getGamesByPlatform('-metacritic', 18).subscribe((gamelist) => { this.games = gamelist.results; });
                        }
                    }, {
                        text: 'PS5',
                        handler: () => {
                            this.http.getGamesByPlatform('-metacritic', 187).subscribe((gamelist) => { this.games = gamelist.results; });
                        }
                    }, {
                        text: 'Xbox One',
                        handler: () => {
                            this.http.getGamesByPlatform('-metacritic', 1).subscribe((gamelist) => { this.games = gamelist.results; });
                        }
                    }, {
                        text: 'Xbox Series X',
                        handler: () => {
                            this.http.getGamesByPlatform('-metacritic', 186).subscribe((gamelist) => { this.games = gamelist.results; });
                        }
                    }, {
                        text: 'Nintendo Switch',
                        handler: () => {
                            this.http.getGamesByPlatform('-metacritic', 7).subscribe((gamelist) => { this.games = gamelist.results; });
                        }
                    }, {
                        text: 'PC',
                        handler: () => {
                            this.http.getGamesByPlatform('-metacritic', 4).subscribe((gamelist) => { this.games = gamelist.results; });
                        }
                    }]
            });
            yield actionSheet.present();
            const { role } = yield actionSheet.onDidDismiss();
            console.log('onDidDismiss resolved with role', role);
        });
    }
    genreSearch() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetCtrl.create({
                header: 'Select Genre',
                cssClass: 'my-custom-class',
                buttons: [{
                        text: 'Action',
                        handler: () => {
                            this.http.getGamesByGenre('-metacritic', 'genres', 'action').subscribe((gamelist) => { this.games = gamelist.results; });
                        }
                    }, {
                        text: 'Adventure',
                        handler: () => {
                            this.http.getGamesByGenre('-metacritic', 'genres', 'adventure').subscribe((gamelist) => { this.games = gamelist.results; });
                        }
                    }, {
                        text: 'RPG',
                        handler: () => {
                            this.http.getGamesByGenre('-metacritic', 'genres', 'role-playing-games-rpg').subscribe((gamelist) => { this.games = gamelist.results; });
                        }
                    }, {
                        text: 'Strategy',
                        handler: () => {
                            this.http.getGamesByGenre('-metacritic', 'genres', 'strategy').subscribe((gamelist) => { this.games = gamelist.results; });
                        }
                    }, {
                        text: 'Shooter',
                        handler: () => {
                            this.http.getGamesByGenre('-metacritic', 'genres', 'shooter').subscribe((gamelist) => { this.games = gamelist.results; });
                        }
                    }, {
                        text: 'Casual',
                        handler: () => {
                            this.http.getGamesByGenre('-metacritic', 'genres', 'casual').subscribe((gamelist) => { this.games = gamelist.results; });
                        }
                    }]
            });
            yield actionSheet.present();
            const { role } = yield actionSheet.onDidDismiss();
            console.log('onDidDismiss resolved with role', role);
        });
    }
    orderBy() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetCtrl.create({
                header: 'Order By',
                cssClass: 'my-custom-class',
                buttons: [{
                        text: 'Name',
                        handler: () => {
                            this.searchGames('name');
                        }
                    }, {
                        text: 'Release Date',
                        handler: () => {
                            this.getActualDate();
                            console.log('2021-01-01,' + this.currentDate);
                            this.http.getGamesByGenre('-released', 'dates', '2021-01-01,' + this.currentDate).subscribe((gamelist) => { this.games = gamelist.results; });
                        }
                    }, {
                        text: 'Rating',
                        handler: () => {
                            this.searchGames('-rating');
                        }
                    }, {
                        text: 'Metacritic',
                        handler: () => {
                            this.searchGames('-metacritic');
                        }
                    }]
            });
            yield actionSheet.present();
            const { role } = yield actionSheet.onDidDismiss();
            console.log('onDidDismiss resolved with role', role);
        });
    }
};
HomePage.ctorParameters = () => [
    { type: src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_7__["AuthenticationService"] },
    { type: src_app_services_videogamescrud_service__WEBPACK_IMPORTED_MODULE_10__["VideogamescrudService"] },
    { type: src_app_services_games_service__WEBPACK_IMPORTED_MODULE_8__["GamesService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ActionSheetController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] },
    { type: src_app_services_localstorage_service__WEBPACK_IMPORTED_MODULE_9__["LocalstorageService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["DomController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Renderer2"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_3__["DatePipe"] }
];
HomePage.propDecorators = {
    ionList: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonList"],] }],
    content: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonContent"],] }]
};
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-home',
        template: _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_home_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module.js.map