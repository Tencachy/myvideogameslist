(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-currently-playing-currently-playing-module"],{

/***/ "AMjd":
/*!*********************************************************************!*\
  !*** ./src/app/pages/currently-playing/currently-playing.module.ts ***!
  \*********************************************************************/
/*! exports provided: CurrentlyPlayingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrentlyPlayingPageModule", function() { return CurrentlyPlayingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _currently_playing_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./currently-playing-routing.module */ "JJ/K");
/* harmony import */ var _currently_playing_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./currently-playing.page */ "VaOu");







let CurrentlyPlayingPageModule = class CurrentlyPlayingPageModule {
};
CurrentlyPlayingPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _currently_playing_routing_module__WEBPACK_IMPORTED_MODULE_5__["CurrentlyPlayingPageRoutingModule"]
        ],
        declarations: [_currently_playing_page__WEBPACK_IMPORTED_MODULE_6__["CurrentlyPlayingPage"]]
    })
], CurrentlyPlayingPageModule);



/***/ }),

/***/ "JJ/K":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/currently-playing/currently-playing-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: CurrentlyPlayingPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrentlyPlayingPageRoutingModule", function() { return CurrentlyPlayingPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _currently_playing_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./currently-playing.page */ "VaOu");




const routes = [
    {
        path: '',
        component: _currently_playing_page__WEBPACK_IMPORTED_MODULE_3__["CurrentlyPlayingPage"]
    }
];
let CurrentlyPlayingPageRoutingModule = class CurrentlyPlayingPageRoutingModule {
};
CurrentlyPlayingPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CurrentlyPlayingPageRoutingModule);



/***/ }),

/***/ "VaOu":
/*!*******************************************************************!*\
  !*** ./src/app/pages/currently-playing/currently-playing.page.ts ***!
  \*******************************************************************/
/*! exports provided: CurrentlyPlayingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CurrentlyPlayingPage", function() { return CurrentlyPlayingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_currently_playing_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./currently-playing.page.html */ "n8f3");
/* harmony import */ var _currently_playing_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./currently-playing.page.scss */ "lumu");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_videogamescrud_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/videogamescrud.service */ "RfOB");






let CurrentlyPlayingPage = class CurrentlyPlayingPage {
    constructor(gamesCrud, router) {
        this.gamesCrud = gamesCrud;
        this.router = router;
    }
    ngOnInit() {
        this.getListGame();
    }
    getListGame() {
        this.gamesCrud.getVideogamesFromFirebase('playing').subscribe((res) => {
            this.videogamesList = res;
            console.log(this.videogamesList);
        });
    }
    removeVideogame(name) {
        if (name !== null) {
            this.gamesCrud.removeSingleVideogame('playing', name);
        }
    }
    openGameDetails(id) {
        this.router.navigate(['detail', id]);
    }
};
CurrentlyPlayingPage.ctorParameters = () => [
    { type: src_app_services_videogamescrud_service__WEBPACK_IMPORTED_MODULE_5__["VideogamescrudService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
CurrentlyPlayingPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-currently-playing',
        template: _raw_loader_currently_playing_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_currently_playing_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], CurrentlyPlayingPage);



/***/ }),

/***/ "lumu":
/*!*********************************************************************!*\
  !*** ./src/app/pages/currently-playing/currently-playing.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".listItem {\n  margin: 10px;\n  border-radius: 12px 12px 12px 12px;\n}\n\n.headerButtons {\n  position: sticky;\n}\n\n.arrow-up {\n  border-radius: 38px 38px 38px 38px;\n  margin: 4px;\n  padding: 4px;\n  background-color: var(--ion-color-primary);\n}\n\n.itemThumbail {\n  height: 80px;\n  width: 120px;\n  margin: 8px;\n}\n\n.itemImage {\n  object-fit: fill;\n}\n\n.itemLabelText {\n  vertical-align: text-top;\n  font-size: 15px;\n  font-weight: bolder;\n}\n\nion-icon {\n  display: inline-block;\n  font-size: 30px;\n  vertical-align: middle;\n}\n\n.iconText {\n  font-size: 12px;\n  font-weight: bolder;\n  display: inline-block;\n  vertical-align: middle;\n}\n\n.filterButtons {\n  padding: 12px;\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  position: sticky;\n  top: 0px;\n  z-index: 1;\n  background-color: var(--ion-color-light);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxjdXJyZW50bHktcGxheWluZy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQ0Msa0NBQUE7QUFDTDs7QUFFQztFQUNJLGdCQUFBO0FBQ0w7O0FBRUM7RUFDSSxrQ0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMENBQUE7QUFDTDs7QUFHQztFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQUFMOztBQUdDO0VBQ0ksZ0JBQUE7QUFBTDs7QUFHQztFQUNJLHdCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FBQUw7O0FBSUM7RUFDSSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtBQURMOztBQUlDO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxzQkFBQTtBQURMOztBQUtDO0VBRUksYUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSx3Q0FBQTtBQUhMIiwiZmlsZSI6ImN1cnJlbnRseS1wbGF5aW5nLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5saXN0SXRlbXtcclxuICAgIG1hcmdpbjogMTBweDtcclxuICAgICBib3JkZXItcmFkaXVzOiAxMnB4IDEycHggMTJweCAxMnB4O1xyXG4gfVxyXG4gXHJcbiAuaGVhZGVyQnV0dG9uc3tcclxuICAgICBwb3NpdGlvbjogc3RpY2t5O1xyXG4gfVxyXG4gXHJcbiAuYXJyb3ctdXB7XHJcbiAgICAgYm9yZGVyLXJhZGl1czogMzhweCAzOHB4IDM4cHggMzhweDtcclxuICAgICBtYXJnaW46NHB4O1xyXG4gICAgIHBhZGRpbmc6IDRweDtcclxuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICAgXHJcbiB9XHJcbiBcclxuIC5pdGVtVGh1bWJhaWx7XHJcbiAgICAgaGVpZ2h0OiA4MHB4O1xyXG4gICAgIHdpZHRoOiAxMjBweDtcclxuICAgICBtYXJnaW46IDhweDsgXHJcbiB9XHJcbiBcclxuIC5pdGVtSW1hZ2V7XHJcbiAgICAgb2JqZWN0LWZpdDpmaWxsOyBcclxuIH1cclxuIFxyXG4gLml0ZW1MYWJlbFRleHR7XHJcbiAgICAgdmVydGljYWwtYWxpZ246IHRleHQtdG9wO1xyXG4gICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICBmb250LXdlaWdodDogYm9sZGVyO1xyXG4gfVxyXG4gXHJcbiBcclxuIGlvbi1pY29uIHtcclxuICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgIH1cclxuIFxyXG4gLmljb25UZXh0e1xyXG4gICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICBmb250LXdlaWdodDogYm9sZGVyO1xyXG4gICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gfVxyXG4gIFxyXG4gXHJcbiAuZmlsdGVyQnV0dG9uc3tcclxuICAgXHJcbiAgICAgcGFkZGluZzogMTJweDtcclxuICAgICB3aWR0aDogMTAwJTtcclxuICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgcG9zaXRpb246IHN0aWNreTtcclxuICAgICB0b3A6MHB4O1xyXG4gICAgIHotaW5kZXg6IDE7XHJcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuIFxyXG4gICAgIFxyXG4gfVxyXG4gIl19 */");

/***/ }),

/***/ "n8f3":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/currently-playing/currently-playing.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title color=\"primary\">Playing</ion-title>\r\n    <ion-buttons color=\"primary\" slot=\"start\">\r\n      <ion-menu-button menu=\"main-menu\" color=\"primary\"></ion-menu-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-list>\r\n    <ion-item-sliding *ngFor=\"let game of videogamesList\" class=\"listItem\"> \r\n      <ion-item-options side=\"end\">\r\n        <ion-item-option (click)=\"removeVideogame(game.videogame.name)\" color=\"light\" >\r\n          <ion-icon slot=\"icon-only\" name=\"trash-outline\"></ion-icon>\r\n          <!--<p class=\"iconText\" color=\"primary\">Remove</p>-->\r\n        </ion-item-option >\r\n      </ion-item-options>\r\n      <ion-item color=\"primary\" (click)=\"openGameDetails(game.videogame.id)\">\r\n      <ion-thumbnail slot=\"start\"  class=\"itemThumbail\" (click)=\"openGameDetails(game.videogame.id)\">\r\n        <img *ngIf=\"game.videogame.background_image\" src=\"{{game.videogame.background_image}}\" class=\"itemImage\" >\r\n        <img *ngIf=\"!game.videogame.background_image\" src=\"./assets/noimage.jpg\" class=\"itemImage\" >\r\n      </ion-thumbnail>\r\n      <ion-label >\r\n        <p class=\"itemLabelText\">{{game.videogame.name}}</p>\r\n        <p class=\"itemLabelText\" >({{game.videogame.released}})</p>\r\n        <p class=\"itemLabelText\" *ngIf=\"game.videogame.rating>0\" >{{game.videogame.rating}}  </p>\r\n        <p class=\"itemLabelText\" *ngIf=\"1>game.videogame.rating\" >---</p>\r\n       <span class=\"itemLabelText\" *ngFor=\"let pf of game.videogame.parent_platforms;let last=last\"  >{{pf.platform.name}} <span *ngIf=\"!last\">-\r\n       </span></span>\r\n      </ion-label>\r\n    </ion-item>\r\n   </ion-item-sliding>\r\n\r\n  </ion-list>\r\n\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=pages-currently-playing-currently-playing-module.js.map