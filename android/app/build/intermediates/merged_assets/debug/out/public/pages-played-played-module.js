(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-played-played-module"],{

/***/ "Gt4F":
/*!***********************************************!*\
  !*** ./src/app/pages/played/played.module.ts ***!
  \***********************************************/
/*! exports provided: PlayedPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayedPageModule", function() { return PlayedPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _played_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./played-routing.module */ "ZuRy");
/* harmony import */ var _played_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./played.page */ "o7kJ");







let PlayedPageModule = class PlayedPageModule {
};
PlayedPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _played_routing_module__WEBPACK_IMPORTED_MODULE_5__["PlayedPageRoutingModule"]
        ],
        declarations: [_played_page__WEBPACK_IMPORTED_MODULE_6__["PlayedPage"]]
    })
], PlayedPageModule);



/***/ }),

/***/ "ZuRy":
/*!*******************************************************!*\
  !*** ./src/app/pages/played/played-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: PlayedPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayedPageRoutingModule", function() { return PlayedPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _played_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./played.page */ "o7kJ");




const routes = [
    {
        path: '',
        component: _played_page__WEBPACK_IMPORTED_MODULE_3__["PlayedPage"]
    }
];
let PlayedPageRoutingModule = class PlayedPageRoutingModule {
};
PlayedPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PlayedPageRoutingModule);



/***/ }),

/***/ "bFUj":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/played/played.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title color=\"primary\">Played</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button color=\"primary\" menu=\"main-menu\"></ion-menu-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <ion-list>\r\n    <ion-item-sliding *ngFor=\"let game of videogamesList\" class=\"listItem\"> \r\n      <ion-item-options side=\"end\">\r\n        <ion-item-option (click)=\"removeVideogame(game.videogame.name)\" color=\"light\" >\r\n          <ion-icon slot=\"icon-only\" name=\"trash-outline\"></ion-icon>\r\n          <!--<p class=\"iconText\" color=\"primary\">Remove</p>-->\r\n        </ion-item-option >\r\n      </ion-item-options>\r\n      <ion-item color=\"primary\" (click)=\"openGameDetails(game.videogame.id)\">\r\n      <ion-thumbnail slot=\"start\"  class=\"itemThumbail\" (click)=\"openGameDetails(game.videogame.id)\">\r\n        <img *ngIf=\"game.videogame.background_image\" src=\"{{game.videogame.background_image}}\" class=\"itemImage\" >\r\n        <img *ngIf=\"!game.videogame.background_image\" src=\"./assets/noimage.jpg\" class=\"itemImage\" >\r\n      </ion-thumbnail>\r\n      <ion-label >\r\n        <p class=\"itemLabelText\">{{game.videogame.name}}</p>\r\n        <p class=\"itemLabelText\" >({{game.videogame.released}})</p>\r\n        <p class=\"itemLabelText\" *ngIf=\"game.videogame.rating>0\" >{{game.videogame.rating}}  </p>\r\n        <p class=\"itemLabelText\" *ngIf=\"1>game.videogame.rating\" >---</p>\r\n       <span class=\"itemLabelText\" *ngFor=\"let pf of game.videogame.parent_platforms;let last=last\"  >{{pf.platform.name}} <span *ngIf=\"!last\">-\r\n       </span></span>\r\n      </ion-label>\r\n    </ion-item>\r\n   </ion-item-sliding>\r\n\r\n  </ion-list>\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "dD2/":
/*!***********************************************!*\
  !*** ./src/app/pages/played/played.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".listItem {\n  margin: 10px;\n  border-radius: 12px 12px 12px 12px;\n}\n\n.headerButtons {\n  position: sticky;\n}\n\n.arrow-up {\n  border-radius: 38px 38px 38px 38px;\n  margin: 4px;\n  padding: 4px;\n  background-color: var(--ion-color-primary);\n}\n\n.itemThumbail {\n  height: 80px;\n  width: 120px;\n  margin: 8px;\n}\n\n.itemImage {\n  object-fit: fill;\n}\n\n.itemLabelText {\n  vertical-align: text-top;\n  font-size: 15px;\n  font-weight: bolder;\n}\n\nion-icon {\n  display: inline-block;\n  font-size: 30px;\n  vertical-align: middle;\n}\n\n.iconText {\n  font-size: 12px;\n  font-weight: bolder;\n  display: inline-block;\n  vertical-align: middle;\n}\n\n.filterButtons {\n  padding: 12px;\n  width: 100%;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  position: sticky;\n  top: 0px;\n  z-index: 1;\n  background-color: var(--ion-color-light);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxwbGF5ZWQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNDLGtDQUFBO0FBQ0w7O0FBRUM7RUFDSSxnQkFBQTtBQUNMOztBQUVDO0VBQ0ksa0NBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDBDQUFBO0FBQ0w7O0FBR0M7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUFBTDs7QUFHQztFQUNJLGdCQUFBO0FBQUw7O0FBR0M7RUFDSSx3QkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQUFMOztBQUlDO0VBQ0kscUJBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7QUFETDs7QUFJQztFQUNJLGVBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0Esc0JBQUE7QUFETDs7QUFLQztFQUVJLGFBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0VBQ0Esd0NBQUE7QUFITCIsImZpbGUiOiJwbGF5ZWQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxpc3RJdGVte1xyXG4gICAgbWFyZ2luOiAxMHB4O1xyXG4gICAgIGJvcmRlci1yYWRpdXM6IDEycHggMTJweCAxMnB4IDEycHg7XHJcbiB9XHJcbiBcclxuIC5oZWFkZXJCdXR0b25ze1xyXG4gICAgIHBvc2l0aW9uOiBzdGlja3k7XHJcbiB9XHJcbiBcclxuIC5hcnJvdy11cHtcclxuICAgICBib3JkZXItcmFkaXVzOiAzOHB4IDM4cHggMzhweCAzOHB4O1xyXG4gICAgIG1hcmdpbjo0cHg7XHJcbiAgICAgcGFkZGluZzogNHB4O1xyXG4gICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgICBcclxuIH1cclxuIFxyXG4gLml0ZW1UaHVtYmFpbHtcclxuICAgICBoZWlnaHQ6IDgwcHg7XHJcbiAgICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgIG1hcmdpbjogOHB4OyBcclxuIH1cclxuIFxyXG4gLml0ZW1JbWFnZXtcclxuICAgICBvYmplY3QtZml0OmZpbGw7IFxyXG4gfVxyXG4gXHJcbiAuaXRlbUxhYmVsVGV4dHtcclxuICAgICB2ZXJ0aWNhbC1hbGlnbjogdGV4dC10b3A7XHJcbiAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbiB9XHJcbiBcclxuIFxyXG4gaW9uLWljb24ge1xyXG4gICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgfVxyXG4gXHJcbiAuaWNvblRleHR7XHJcbiAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbiAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiB9XHJcbiAgXHJcbiBcclxuIC5maWx0ZXJCdXR0b25ze1xyXG4gICBcclxuICAgICBwYWRkaW5nOiAxMnB4O1xyXG4gICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICBwb3NpdGlvbjogc3RpY2t5O1xyXG4gICAgIHRvcDowcHg7XHJcbiAgICAgei1pbmRleDogMTtcclxuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gXHJcbiAgICAgXHJcbiB9XHJcbiAiXX0= */");

/***/ }),

/***/ "o7kJ":
/*!*********************************************!*\
  !*** ./src/app/pages/played/played.page.ts ***!
  \*********************************************/
/*! exports provided: PlayedPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayedPage", function() { return PlayedPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_played_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./played.page.html */ "bFUj");
/* harmony import */ var _played_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./played.page.scss */ "dD2/");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_videogamescrud_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/videogamescrud.service */ "RfOB");







let PlayedPage = class PlayedPage {
    constructor(router, gamesCrud) {
        this.router = router;
        this.gamesCrud = gamesCrud;
    }
    ngOnInit() {
        this.getListGame();
    }
    getListGame() {
        this.gamesCrud.getVideogamesFromFirebase('played').subscribe((res) => {
            this.videogamesList = res;
            console.log(this.videogamesList);
        });
    }
    removeVideogame(name) {
        if (name !== null) {
            this.gamesCrud.removeSingleVideogame('played', name);
        }
    }
    openGameDetails(id) {
        this.router.navigate(['detail', id]);
    }
};
PlayedPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_videogamescrud_service__WEBPACK_IMPORTED_MODULE_6__["VideogamescrudService"] }
];
PlayedPage.propDecorators = {
    ionList: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonList"],] }]
};
PlayedPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-played',
        template: _raw_loader_played_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_played_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], PlayedPage);



/***/ })

}]);
//# sourceMappingURL=pages-played-played-module.js.map