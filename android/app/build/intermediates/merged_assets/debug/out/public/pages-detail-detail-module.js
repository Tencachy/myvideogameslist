(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detail-detail-module"],{

/***/ "815U":
/*!*******************************************************!*\
  !*** ./src/app/pages/detail/detail-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: DetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailPageRoutingModule", function() { return DetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detail.page */ "bFDb");




const routes = [
    {
        path: '',
        component: _detail_page__WEBPACK_IMPORTED_MODULE_3__["DetailPage"]
    }
];
let DetailPageRoutingModule = class DetailPageRoutingModule {
};
DetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DetailPageRoutingModule);



/***/ }),

/***/ "CKmm":
/*!***********************************************!*\
  !*** ./src/app/pages/detail/detail.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".myImg {\n  max-width: 280px;\n  max-height: 180;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0FBQ0oiLCJmaWxlIjoiZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5teUltZyB7XHJcbiAgICBtYXgtd2lkdGg6IDI4MHB4O1xyXG4gICAgbWF4LWhlaWdodDogMTgwO1xyXG59Il19 */");

/***/ }),

/***/ "Up1C":
/*!***********************************************!*\
  !*** ./src/app/pages/detail/detail.module.ts ***!
  \***********************************************/
/*! exports provided: DetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailPageModule", function() { return DetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detail-routing.module */ "815U");
/* harmony import */ var _detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detail.page */ "bFDb");







let DetailPageModule = class DetailPageModule {
};
DetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["DetailPageRoutingModule"]
        ],
        declarations: [_detail_page__WEBPACK_IMPORTED_MODULE_6__["DetailPage"]]
    })
], DetailPageModule);



/***/ }),

/***/ "bFDb":
/*!*********************************************!*\
  !*** ./src/app/pages/detail/detail.page.ts ***!
  \*********************************************/
/*! exports provided: DetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailPage", function() { return DetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_detail_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./detail.page.html */ "oE3O");
/* harmony import */ var _detail_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./detail.page.scss */ "CKmm");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_games_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/games.service */ "wZ2L");
/* harmony import */ var src_app_services_videogamescrud_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/videogamescrud.service */ "RfOB");









let DetailPage = class DetailPage {
    constructor(dom, activatedRoute, http, videogameService) {
        this.dom = dom;
        this.activatedRoute = activatedRoute;
        this.http = http;
        this.videogameService = videogameService;
        this.segment = 0;
    }
    ngOnInit() {
        this.gameId = this.activatedRoute.snapshot.paramMap.get('id');
        this.http.getGameDetails(this.gameId).subscribe(res => {
            this.game = res;
            this.getVideogamesFromLists();
        });
        this.getGameScreenshots(this.gameId);
    }
    ngOnDestroy() {
        if (this.gameSub) {
            this.gameSub.unsubscribe();
        }
        if (this.routeSub) {
            this.routeSub.unsubscribe();
        }
    }
    getGameDetails(id) {
        this.gameSub = this.http
            .getGameDetails(id)
            .subscribe((gameResp) => {
            this.game = gameResp;
            setTimeout(() => {
            }, 1000);
        });
    }
    getGameScreenshots(id) {
        this.http.getGameScreenshots(id).subscribe((gameList) => {
            this.screen = gameList.results;
        });
    }
    getGameTrailers(id) {
        this.http.getGameTrailers(id).subscribe((gameList) => {
            this.trailers = gameList.results;
        });
    }
    segmentChanged() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.slider.slideTo(this.segment);
        });
    }
    slideChanged() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.segment = yield this.slider.getActiveIndex();
            this.goTop();
        });
    }
    goTop() {
        this.content.scrollToTop(500);
    }
    getVideogamesFromLists() {
        this.videogameService.getVideogamesFromFirebase('wantToPlay').subscribe((res) => {
            this.videogamesListWantToPlay = res;
            this.compareVideogameName(this.videogamesListWantToPlay, 'wantToPlay');
        });
        this.videogameService.getVideogamesFromFirebase('playing').subscribe((res) => {
            this.videogamesListPlaying = res;
            this.compareVideogameName(this.videogamesListPlaying, 'playing');
        });
        this.videogameService.getVideogamesFromFirebase('played').subscribe((res) => {
            this.videogamesListPlayed = res;
            this.compareVideogameName(this.videogamesListPlayed, 'played');
        });
    }
    compareVideogameName(list, path) {
        const firebaselist = list;
        for (var i = 0; i < list.length; i++) {
            if (this.game.name === firebaselist[i].videogame.name) {
                this.game.state = path;
                this.game.state === 'wantToPlay' ? this.game.optionColorWantToPlay = 'tertiary' : this.game.optionColorWantToPlay = 'secondary';
                this.game.state === 'playing' ? this.game.optionColorPlaying = 'tertiary' : this.game.optionColorPlaying = 'secondary';
                this.game.state === 'played' ? this.game.optionColorPlayed = 'tertiary' : this.game.optionColorPlayed = 'secondary';
            }
            else {
                this.game.state === 'wantToPlay' ? this.game.optionColorWantToPlay = 'tertiary' : this.game.optionColorWantToPlay = 'secondary';
                this.game.state === 'playing' ? this.game.optionColorPlaying = 'tertiary' : this.game.optionColorPlaying = 'secondary';
                this.game.state === 'played' ? this.game.optionColorPlayed = 'tertiary' : this.game.optionColorPlayed = 'secondary';
            }
        }
    }
    storeVideogame(list) {
        this.videogameService.pushVideogame(this.game, list);
        this.getVideogamesFromLists();
    }
};
DetailPage.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["DomSanitizer"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: src_app_services_games_service__WEBPACK_IMPORTED_MODULE_7__["GamesService"] },
    { type: src_app_services_videogamescrud_service__WEBPACK_IMPORTED_MODULE_8__["VideogamescrudService"] }
];
DetailPage.propDecorators = {
    slider: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['slides',] }],
    content: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonContent"],] }]
};
DetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-detail',
        template: _raw_loader_detail_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_detail_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], DetailPage);



/***/ }),

/***/ "oE3O":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/detail/detail.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button text=\"\" defaultHref=\"home\" color=\"primary\" mode=\"ios\">\r\n      </ion-back-button>\r\n    </ion-buttons>\r\n    <div *ngIf=\"game\">\r\n      <ion-title color=\"primary\">{{game.name}}</ion-title>\r\n    </div>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"game\">\r\n  <div>\r\n    <ion-card color=\"primary\">\r\n      <ion-card-content class=\"ion-text-center\">\r\n        <img src=\"{{game.background_image}}\" alt=\"\">\r\n        <ion-card-title>{{game.name}}</ion-card-title>\r\n        <ion-card-subtitle>{{game.rating}} / 5</ion-card-subtitle>\r\n        <div>   \r\n           <ion-button fill=\"solid\" shape=\"round\" size=\"medium\" (click)=\"storeVideogame('wantToPlay')\" [color]=\"game.optionColorWantToPlay\">\r\n         Want To Play\r\n        </ion-button>\r\n        <ion-button fill=\"solid\" shape=\"round\" size=\"medium\" (click)=\"storeVideogame('playing')\"  [color]=\"game.optionColorPlaying\" >\r\n          Playing\r\n        </ion-button>\r\n        <ion-button fill=\"solid\" shape=\"round\" size=\"medium\" (click)=\"storeVideogame('played')\" [color]=\"game.optionColorPlayed\" >\r\n          Played\r\n        </ion-button></div>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </div>\r\n  \r\n  <ion-slides #slides (ionSlideDidChange)=\"slideChanged()\"  pager=\"false\">\r\n    <ion-slide >\r\n      <ion-card>\r\n        <ion-card-content class=\"ion-text-center\">\r\n          <ion-card-title>Description</ion-card-title>\r\n          <div innerHTML=\"{{game.description}}\"></div>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </ion-slide>\r\n    <ion-slide>\r\n      <ion-card class=\"ion-text-center\">\r\n        <ion-card-title>Information</ion-card-title>\r\n      <ion-grid>\r\n        <ion-row>\r\n          <ion-col>\r\n            <div><p>Release date</p></div>\r\n            <div><p style=\"font-size:14px;\">{{game.released}}.</p></div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col>\r\n            <div><p>Genres</p></div>\r\n            <div *ngFor=\"let genre of game.genres\">\r\n            <div><ion-button color=\"primary\" size=\"small\" shape=\"round\" disabled=\"true\">{{genre.name}} </ion-button></div>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col>\r\n            <div><p>Platforms</p></div>\r\n            <div *ngFor=\"let pf of game.parent_platforms\">\r\n              \r\n                <div ><ion-button  size=\"small\" shape=\"round\" disabled=\"true\">{{pf.platform.name}}</ion-button></div>\r\n            </div>\r\n\r\n          </ion-col>\r\n          <ion-col>\r\n            <div><p>Publishers</p></div>\r\n            <div *ngFor=\"let pub of game.publishers\">\r\n            <div><ion-button size=\"small\" shape=\"round\" disabled=\"true\">{{pub.name}} </ion-button></div>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </ion-card>\r\n    </ion-slide>\r\n    <ion-slide>\r\n\r\n      <ion-grid>\r\n        <ion-row>\r\n          <ion-col *ngIf=\"screen?.length\" >\r\n         <div>\r\n           <img *ngFor=\"let sc of screen\" class=\"myImg\" src=\"{{sc.image}}\" >\r\n          \r\n           </div>\r\n           </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n\r\n    </ion-slide>\r\n  </ion-slides>\r\n</ion-content>\r\n\r\n\r\n<ion-footer class=\"ion-no-border\">\r\n  <ion-toolbar >\r\n    <ion-segment (ionChange)=\"segmentChanged()\" [(ngModel)]=\"segment\" color=\"dark\">\r\n      <ion-segment-button value=\"0\">\r\n        <ion-label color=\"primary\">Description</ion-label>\r\n      </ion-segment-button>\r\n      <ion-segment-button value=\"1\">\r\n        <ion-label color=\"primary\">Information</ion-label>\r\n      </ion-segment-button>\r\n      <ion-segment-button value=\"2\" color=\"primary\" *ngIf=\"screen?.length\"> \r\n        <ion-label color=\"primary\">Screenshots</ion-label>\r\n      </ion-segment-button>\r\n    </ion-segment>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n");

/***/ })

}]);
//# sourceMappingURL=pages-detail-detail-module.js.map