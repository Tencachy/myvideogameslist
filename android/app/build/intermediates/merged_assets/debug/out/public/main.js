(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! G:\PROYECTOS_IONIC\MyVideogamesList\src\main.ts */"zUnb");


/***/ }),

/***/ "8Qyz":
/*!**********************************************************!*\
  !*** ./src/app/interceptors/http-headers.interceptor.ts ***!
  \**********************************************************/
/*! exports provided: HttpHeadersInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpHeadersInterceptor", function() { return HttpHeadersInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");


let HttpHeadersInterceptor = class HttpHeadersInterceptor {
    constructor() { }
    intercept(req, next) {
        req = req.clone({
            setHeaders: {
                "x-rapidapi-key": "f1c527b534msh9ca68b2c5480a0cp1296fbjsnda3aee070ee9",
                "x-rapidapi-host": "rawg-video-games-database.p.rapidapi.com",
            },
            setParams: {
                key: 'e60c45cc6e3d47c49ba21ee9eaed45d1'
            }
        });
        return next.handle(req);
    }
};
HttpHeadersInterceptor.ctorParameters = () => [];
HttpHeadersInterceptor = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], HttpHeadersInterceptor);



/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    BASE_URL: 'https://rawg-video-games-database.p.rapidapi.com'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "FAUO":
/*!**************************************************!*\
  !*** ./src/app/services/localstorage.service.ts ***!
  \**************************************************/
/*! exports provided: LocalstorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalstorageService", function() { return LocalstorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");


let LocalstorageService = class LocalstorageService {
    constructor() { }
    set(key, value) {
        try {
            localStorage.setItem(key, value);
        }
        catch (e) {
            console.log(e);
        }
    }
    get(key) {
        try {
            return localStorage.getItem(key);
        }
        catch (e) {
            console.log(e);
        }
    }
};
LocalstorageService.ctorParameters = () => [];
LocalstorageService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LocalstorageService);



/***/ }),

/***/ "LFOy":
/*!********************************************!*\
  !*** ./src/app/guards/auto-login.guard.ts ***!
  \********************************************/
/*! exports provided: AUTHENTICATION_KEY, AutoLoginGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AUTHENTICATION_KEY", function() { return AUTHENTICATION_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutoLoginGuard", function() { return AutoLoginGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _services_localstorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/localstorage.service */ "FAUO");



const AUTHENTICATION_KEY = 'authenticated';

let AutoLoginGuard = class AutoLoginGuard {
    constructor(localStrg, 
    /*private storage: Storage, */
    router) {
        this.localStrg = localStrg;
        this.router = router;
    }
    canLoad() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.localStrg.get(AUTHENTICATION_KEY) === 'true') {
                this.router.navigateByUrl('/home', { replaceUrl: true });
                return true;
            }
            else {
                return true;
            }
        });
    }
};
AutoLoginGuard.ctorParameters = () => [
    { type: _services_localstorage_service__WEBPACK_IMPORTED_MODULE_3__["LocalstorageService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
AutoLoginGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AutoLoginGuard);



/***/ }),

/***/ "RfOB":
/*!****************************************************!*\
  !*** ./src/app/services/videogamescrud.service.ts ***!
  \****************************************************/
/*! exports provided: VideogamescrudService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideogamescrudService", function() { return VideogamescrudService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "sSZD");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./authentication.service */ "ej43");





let VideogamescrudService = class VideogamescrudService {
    constructor(auth, database, toastController) {
        this.auth = auth;
        this.database = database;
        this.toastController = toastController;
    }
    pushVideogame(videogame, list) {
        if (this.auth.userID !== null && this.auth.userID !== undefined) {
            let strings = ['wantToPlay', 'played', 'playing'];
            strings.forEach((element, index) => {
                if (element === list)
                    strings.splice(index, 1);
            });
            for (let listName of strings) {
                this.removeSingleVideogame(listName, videogame.name);
            }
            this.videogameName = videogame.name;
            this.firebaseRoute = `Users/${this.auth.userID}/${list}/${this.videogameName}`;
            console.log(this.firebaseRoute);
            this.database.object(this.firebaseRoute).set({
                videogame
            }).then(() => {
                console.log("Game uploaded");
                this.gameUploadedToast(list);
            });
        }
    }
    gameUploadedToast(list) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: `${this.videogameName} added to ${list}`,
                duration: 1500,
                cssClass: "my-toast"
            });
            toast.present();
        });
    }
    getVideogamesFromFirebase(list) {
        return this.videogamesListPlayed = this.database.list(`Users/${this.auth.userID}/${list}`).valueChanges();
    }
    getVideogameName(videogame) {
        this.videogameName = videogame.name;
        console.log("CRUD " + this.videogameName);
    }
    removeSingleVideogame(list, name) {
        let gamePath = this.database.object(`Users/${this.auth.userID}/${list}/${name}`);
        gamePath.remove().then(() => {
            this.gameRemovedToast(name, list);
        });
    }
    gameRemovedToast(name, list) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: `${name} removed from ${list}`,
                duration: 1500,
                cssClass: "my-toast"
            });
            toast.present();
        });
    }
};
VideogamescrudService.ctorParameters = () => [
    { type: _authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] }
];
VideogamescrudService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], VideogamescrudService);



/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./app.component.html */ "VzVu");
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component.scss */ "ynWL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _guards_auto_login_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./guards/auto-login.guard */ "LFOy");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/authentication.service */ "ej43");
/* harmony import */ var _services_localstorage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/localstorage.service */ "FAUO");







let AppComponent = class AppComponent {
    constructor(localStrg, authentication) {
        this.localStrg = localStrg;
        this.authentication = authentication;
        this.profileimage = '../assets/profile_light.png';
        this.isAuthenticated = false;
        this.checkDarkTheme();
    }
    logout() {
        this.authentication.logout();
    }
    checkAuthentication() {
        if (this.localStrg.get(_guards_auto_login_guard__WEBPACK_IMPORTED_MODULE_4__["AUTHENTICATION_KEY"]) === 'true') {
            this.isAuthenticated = true;
        }
        else {
            this.isAuthenticated = false;
        }
    }
    checkDarkTheme() {
        const preferDark = window.matchMedia('(prefers-color-schema:dark)');
        if (preferDark.matches) {
            document.body.classList.toggle('dark');
        }
    }
    getUsername() {
        if (this.isAuthenticated) {
            this.username = this.authentication.user.username;
        }
    }
    changeProfileImage() {
        if (this.dark_theme) {
            this.profileimage = '../assets/profile_dark.png';
        }
        else {
            this.profileimage = '../assets/profile_light.png';
        }
    }
    openSideMenu() {
        this.authentication.getUser();
        this.checkAuthentication();
        this.changeProfileImage();
        this.getUsername();
    }
};
AppComponent.ctorParameters = () => [
    { type: _services_localstorage_service__WEBPACK_IMPORTED_MODULE_6__["LocalstorageService"] },
    { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"] }
];
AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], AppComponent);



/***/ }),

/***/ "UTcu":
/*!**************************************!*\
  !*** ./src/app/guards/auth.guard.ts ***!
  \**************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _guards_auto_login_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../guards/auto-login.guard */ "LFOy");
/* harmony import */ var _services_localstorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/localstorage.service */ "FAUO");





let AuthGuard = class AuthGuard {
    constructor(localStrg, 
    /*private storage: Storage,*/
    router) {
        this.localStrg = localStrg;
        this.router = router;
    }
    canLoad() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.localStrg.get(_guards_auto_login_guard__WEBPACK_IMPORTED_MODULE_3__["AUTHENTICATION_KEY"]) === 'true') {
                return true;
            }
            else {
                this.router.navigateByUrl('/login', { replaceUrl: true });
                return true;
            }
        });
    }
};
AuthGuard.ctorParameters = () => [
    { type: _services_localstorage_service__WEBPACK_IMPORTED_MODULE_4__["LocalstorageService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
AuthGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthGuard);



/***/ }),

/***/ "VzVu":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\r\n  <ion-menu (ionWillOpen)=\"openSideMenu()\" menuId=\"main-menu\" contentId=\"main\">\r\n    <ion-header class=\"ion-no-border\">\r\n      <ion-toolbar>\r\n        <!--<div class=\"menu-circle\"></div>-->\r\n        <div class=\"image-container\">\r\n          <span class=\"helper\"></span>\r\n          <img src=\"{{profileimage}}\">\r\n        </div>\r\n        <ion-title  color=\"primary\" class=\"ion-text-center\">{{ isAuthenticated? username : 'Log in' }}</ion-title>\r\n      </ion-toolbar>\r\n    </ion-header>\r\n    <ion-content>\r\n      \r\n      <ion-list>\r\n        <ion-menu-toggle>\r\n          <ion-item routerLink=\"profile\">\r\n            <ion-icon color=\"primary\" name=\"person-outline\" slot=\"start\"></ion-icon>\r\n            <ion-label  color=\"primary\">Profile</ion-label>\r\n          </ion-item>\r\n        </ion-menu-toggle>\r\n\r\n        <ion-menu-toggle>\r\n          <ion-item routerLink=\"home\">\r\n            <ion-icon  color=\"primary\" name=\"home-outline\" slot=\"start\"></ion-icon>\r\n            <ion-label  color=\"primary\">Home</ion-label>\r\n          </ion-item>\r\n        </ion-menu-toggle>\r\n\r\n        <ion-menu-toggle>\r\n          <ion-item routerLink=\"want-to-play\">\r\n            <ion-icon  color=\"primary\" name=\"locate-outline\" slot=\"start\"></ion-icon>\r\n            <ion-label  color=\"primary\">Want to play</ion-label>\r\n          </ion-item>\r\n        </ion-menu-toggle>\r\n\r\n        <ion-menu-toggle>\r\n          <ion-item routerLink=\"currently-playing\">\r\n            <ion-icon  color=\"primary\" name=\"game-controller-outline\" slot=\"start\"></ion-icon>\r\n            <ion-label  color=\"primary\">Currently playing</ion-label>\r\n          </ion-item>\r\n        </ion-menu-toggle>\r\n        \r\n        <ion-menu-toggle>\r\n          <ion-item routerLink=\"played\">\r\n            <ion-icon  color=\"primary\" name=\"lock-closed-outline\" slot=\"start\"></ion-icon>\r\n            <ion-label  color=\"primary\">Played</ion-label>\r\n          </ion-item>\r\n        </ion-menu-toggle>\r\n        \r\n        <ion-menu-toggle>\r\n          <ion-item routerLink=\"top-games\">\r\n            <ion-icon  color=\"primary\" name=\"trophy-outline\" slot=\"start\"></ion-icon>\r\n            <ion-label  color=\"primary\">Top games</ion-label>\r\n          </ion-item>\r\n        </ion-menu-toggle>\r\n\r\n        <ion-menu-toggle>\r\n          <ion-item routerLink=\"settings\">\r\n            <ion-icon  color=\"primary\" name=\"settings-outline\" slot=\"start\"></ion-icon>\r\n            <ion-label  color=\"primary\">Themes</ion-label>\r\n          </ion-item>\r\n        </ion-menu-toggle>\r\n\r\n        <ion-menu-toggle>\r\n          <ion-item (click)=\"logout()\" *ngIf=\"isAuthenticated\">\r\n            <ion-icon  color=\"primary\" name=\"log-out-outline\" slot=\"start\"></ion-icon>\r\n            <ion-label color=\"primary\" >Logout</ion-label>\r\n          </ion-item>\r\n        </ion-menu-toggle>\r\n      </ion-list>\r\n    </ion-content>\r\n  </ion-menu>\r\n  <ion-router-outlet id=\"main\"></ion-router-outlet>\r\n</ion-app>\r\n");

/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _interceptors_http_headers_interceptor__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./interceptors/http-headers.interceptor */ "8Qyz");
/* harmony import */ var _interceptors_http_errors_interceptor__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./interceptors/http-errors.interceptor */ "gU1t");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/fire */ "spgP");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/fire/auth */ "UbJi");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/fire/storage */ "Vaw3");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/fire/database */ "sSZD");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./services/authentication.service */ "ej43");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! src/environments/environment.prod */ "cxbk");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./guards/auth.guard */ "UTcu");
/* harmony import */ var _guards_auto_login_guard__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./guards/auto-login.guard */ "LFOy");
/* harmony import */ var _services_localstorage_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./services/localstorage.service */ "FAUO");
/* harmony import */ var _services_videogamescrud_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./services/videogamescrud.service */ "RfOB");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/common */ "ofXK");
























let AppModule = class AppModule {
};
AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]],
        entryComponents: [],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"],
            _angular_fire__WEBPACK_IMPORTED_MODULE_11__["AngularFireModule"].initializeApp(src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_17__["environment"].firebaseConfig),
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_12__["AngularFirestoreModule"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_13__["AngularFireAuthModule"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_14__["AngularFireStorageModule"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_15__["AngularFireDatabaseModule"],
        ],
        providers: [
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"] },
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HTTP_INTERCEPTORS"], useClass: _interceptors_http_headers_interceptor__WEBPACK_IMPORTED_MODULE_8__["HttpHeadersInterceptor"], multi: true },
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HTTP_INTERCEPTORS"], useClass: _interceptors_http_errors_interceptor__WEBPACK_IMPORTED_MODULE_9__["HttpErrorsInterceptor"], multi: true },
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_16__["AuthenticationService"],
            _guards_auth_guard__WEBPACK_IMPORTED_MODULE_18__["AuthGuard"],
            _guards_auto_login_guard__WEBPACK_IMPORTED_MODULE_19__["AutoLoginGuard"],
            _services_localstorage_service__WEBPACK_IMPORTED_MODULE_20__["LocalstorageService"],
            _services_videogamescrud_service__WEBPACK_IMPORTED_MODULE_21__["VideogamescrudService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_22__["DatePipe"],
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]],
        schemas: [
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]
        ],
    })
], AppModule);



/***/ }),

/***/ "cxbk":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
const environment = {
    production: true,
    BASE_URL: 'https://rawg-video-games-database.p.rapidapi.com',
    firebaseConfig: {
        apiKey: "AIzaSyAcYsYtJStw08jQh08nMKUkVr0NHYDX6G0",
        authDomain: "myvideogameslist-12ffc.firebaseapp.com",
        projectId: "myvideogameslist-12ffc",
        storageBucket: "myvideogameslist-12ffc.appspot.com",
        messagingSenderId: "817541175485",
        appId: "1:817541175485:web:6301c35b3fbe15433bffd4"
    }
};


/***/ }),

/***/ "ej43":
/*!****************************************************!*\
  !*** ./src/app/services/authentication.service.ts ***!
  \****************************************************/
/*! exports provided: LAST_USER_ID, AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LAST_USER_ID", function() { return LAST_USER_ID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "UbJi");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/database */ "sSZD");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _guards_auto_login_guard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../guards/auto-login.guard */ "LFOy");
/* harmony import */ var _localstorage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./localstorage.service */ "FAUO");







const LAST_USER_ID = 'userid';
let AuthenticationService = class AuthenticationService {
    constructor(localStrg, 
    /*private storage: Storage, */
    auth, router, database) {
        this.localStrg = localStrg;
        this.auth = auth;
        this.router = router;
        this.database = database;
    }
    register(user) {
        if (user.email && user.password) {
            this.auth.createUserWithEmailAndPassword(user.email, user.password).then((res) => {
                console.log("Register");
                this.database.object('Users/' + res.user.uid).set({
                    username: user.username,
                    email: user.email,
                    createdAt: Date.now()
                }).then(() => {
                    this.setUserAuthenticatedTrue();
                    this.router.navigateByUrl('/home');
                });
            }).catch(e => {
                console.log(e);
            });
        }
    }
    login(user) {
        if (user.email && user.password) {
            this.auth.signInWithEmailAndPassword(user.email, user.password).then((res) => {
                console.log("Login");
                this.setUserAuthenticatedTrue();
                /*this.isAuthenticated = true;*/
                this.getCurrentUserId();
                this.getUser();
                this.router.navigateByUrl('/home');
            }).catch(e => {
                console.log(e);
            });
        }
    }
    logout() {
        this.auth.signOut().then((res) => {
            console.log("Logout");
            this.userID = null;
            this.setUserAuthenticatedFalse();
            /*this.isAuthenticated = false;*/
            this.router.navigateByUrl('/login');
        }).catch(e => {
            console.log(e);
        });
    }
    resetPassword(email) {
        if (email) {
            this.auth.sendPasswordResetEmail(email).then((res) => {
                console.log("Email reset password");
                this.router.navigateByUrl('/login');
            }).catch(e => {
                console.log(e);
            });
        }
    }
    getCurrentUserId() {
        if (this.localStrg.get(_guards_auto_login_guard__WEBPACK_IMPORTED_MODULE_5__["AUTHENTICATION_KEY"]) === 'true') {
            this.auth.user.subscribe((res) => {
                this.userID = res.uid;
                this.localStrg.set(LAST_USER_ID, this.userID);
                console.log("USER ID: " + this.userID);
            });
        }
        else {
            return null;
        }
    }
    setUserAuthenticatedTrue() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.localStrg.set(_guards_auto_login_guard__WEBPACK_IMPORTED_MODULE_5__["AUTHENTICATION_KEY"], 'true');
        });
    }
    setUserAuthenticatedFalse() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.localStrg.set(_guards_auto_login_guard__WEBPACK_IMPORTED_MODULE_5__["AUTHENTICATION_KEY"], 'false');
        });
    }
    getUser() {
        if (this.localStrg.get(_guards_auto_login_guard__WEBPACK_IMPORTED_MODULE_5__["AUTHENTICATION_KEY"]) === 'true') {
            this.database.object(`Users/${this.localStrg.get(LAST_USER_ID)}`).valueChanges().subscribe((res) => {
                this.user = res;
            });
        }
    }
};
AuthenticationService.ctorParameters = () => [
    { type: _localstorage_service__WEBPACK_IMPORTED_MODULE_6__["LocalstorageService"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"] }
];
AuthenticationService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthenticationService);



/***/ }),

/***/ "gU1t":
/*!*********************************************************!*\
  !*** ./src/app/interceptors/http-errors.interceptor.ts ***!
  \*********************************************************/
/*! exports provided: HttpErrorsInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpErrorsInterceptor", function() { return HttpErrorsInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "kU1M");




let HttpErrorsInterceptor = class HttpErrorsInterceptor {
    constructor() { }
    intercept(req, next) {
        return next.handle(req).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(err);
        }));
    }
};
HttpErrorsInterceptor.ctorParameters = () => [];
HttpErrorsInterceptor = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], HttpErrorsInterceptor);



/***/ }),

/***/ "kLfG":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet.entry.js": [
		"dUtr",
		"common",
		0
	],
	"./ion-alert.entry.js": [
		"Q8AI",
		"common",
		1
	],
	"./ion-app_8.entry.js": [
		"hgI1",
		"common",
		2
	],
	"./ion-avatar_3.entry.js": [
		"CfoV",
		"common",
		3
	],
	"./ion-back-button.entry.js": [
		"Nt02",
		"common",
		4
	],
	"./ion-backdrop.entry.js": [
		"Q2Bp",
		5
	],
	"./ion-button_2.entry.js": [
		"0Pbj",
		"common",
		6
	],
	"./ion-card_5.entry.js": [
		"ydQj",
		"common",
		7
	],
	"./ion-checkbox.entry.js": [
		"4fMi",
		"common",
		8
	],
	"./ion-chip.entry.js": [
		"czK9",
		"common",
		9
	],
	"./ion-col_3.entry.js": [
		"/CAe",
		10
	],
	"./ion-datetime_3.entry.js": [
		"WgF3",
		"common",
		11
	],
	"./ion-fab_3.entry.js": [
		"uQcF",
		"common",
		12
	],
	"./ion-img.entry.js": [
		"wHD8",
		13
	],
	"./ion-infinite-scroll_2.entry.js": [
		"2lz6",
		14
	],
	"./ion-input.entry.js": [
		"ercB",
		"common",
		15
	],
	"./ion-item-option_3.entry.js": [
		"MGMP",
		"common",
		16
	],
	"./ion-item_8.entry.js": [
		"9bur",
		"common",
		17
	],
	"./ion-loading.entry.js": [
		"cABk",
		"common",
		18
	],
	"./ion-menu_3.entry.js": [
		"kyFE",
		"common",
		19
	],
	"./ion-modal.entry.js": [
		"TvZU",
		"common",
		20
	],
	"./ion-nav_2.entry.js": [
		"vnES",
		"common",
		21
	],
	"./ion-popover.entry.js": [
		"qCuA",
		"common",
		22
	],
	"./ion-progress-bar.entry.js": [
		"0tOe",
		"common",
		23
	],
	"./ion-radio_2.entry.js": [
		"h11V",
		"common",
		24
	],
	"./ion-range.entry.js": [
		"XGij",
		"common",
		25
	],
	"./ion-refresher_2.entry.js": [
		"nYbb",
		"common",
		26
	],
	"./ion-reorder_2.entry.js": [
		"smMY",
		"common",
		27
	],
	"./ion-ripple-effect.entry.js": [
		"STjf",
		28
	],
	"./ion-route_4.entry.js": [
		"k5eQ",
		"common",
		29
	],
	"./ion-searchbar.entry.js": [
		"OR5t",
		"common",
		30
	],
	"./ion-segment_2.entry.js": [
		"fSgp",
		"common",
		31
	],
	"./ion-select_3.entry.js": [
		"lfGF",
		"common",
		32
	],
	"./ion-slide_2.entry.js": [
		"5xYT",
		33
	],
	"./ion-spinner.entry.js": [
		"nI0H",
		"common",
		34
	],
	"./ion-split-pane.entry.js": [
		"NAQR",
		35
	],
	"./ion-tab-bar_2.entry.js": [
		"knkW",
		"common",
		36
	],
	"./ion-tab_2.entry.js": [
		"TpdJ",
		"common",
		37
	],
	"./ion-text.entry.js": [
		"ISmu",
		"common",
		38
	],
	"./ion-textarea.entry.js": [
		"U7LX",
		"common",
		39
	],
	"./ion-toast.entry.js": [
		"L3sA",
		"common",
		40
	],
	"./ion-toggle.entry.js": [
		"IUOf",
		"common",
		41
	],
	"./ion-virtual-scroll.entry.js": [
		"8Mb5",
		42
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "kLfG";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./guards/auth.guard */ "UTcu");
/* harmony import */ var _guards_auto_login_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./guards/auto-login.guard */ "LFOy");





const routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        loadChildren: () => Promise.all(/*! import() | pages-home-home-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-home-home-module")]).then(__webpack_require__.bind(null, /*! ./pages/home/home.module */ "99Un")).then(m => m.HomePageModule)
    },
    {
        path: 'search/:game-search',
        loadChildren: () => Promise.all(/*! import() | pages-home-home-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-home-home-module")]).then(__webpack_require__.bind(null, /*! ./pages/home/home.module */ "99Un")).then(m => m.HomePageModule)
    },
    {
        path: 'detail/:id',
        loadChildren: () => Promise.all(/*! import() | pages-detail-detail-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-detail-detail-module")]).then(__webpack_require__.bind(null, /*! ./pages/detail/detail.module */ "Up1C")).then(m => m.DetailPageModule)
    },
    {
        path: 'login',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-login-login-module */ "pages-login-login-module").then(__webpack_require__.bind(null, /*! ./pages/login/login.module */ "F4UR")).then(m => m.LoginPageModule),
        canLoad: [_guards_auto_login_guard__WEBPACK_IMPORTED_MODULE_4__["AutoLoginGuard"]]
    },
    {
        path: 'register',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-register-register-module */ "pages-register-register-module").then(__webpack_require__.bind(null, /*! ./pages/register/register.module */ "fhSy")).then(m => m.RegisterPageModule)
    },
    {
        path: 'profile',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-profile-profile-module */ "pages-profile-profile-module").then(__webpack_require__.bind(null, /*! ./pages/profile/profile.module */ "723k")).then(m => m.ProfilePageModule),
        canLoad: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
    },
    {
        path: 'want-to-play',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-want-to-play-want-to-play-module */ "pages-want-to-play-want-to-play-module").then(__webpack_require__.bind(null, /*! ./pages/want-to-play/want-to-play.module */ "OnTo")).then(m => m.WantToPlayPageModule),
        canLoad: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
    },
    {
        path: 'currently-playing',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-currently-playing-currently-playing-module */ "pages-currently-playing-currently-playing-module").then(__webpack_require__.bind(null, /*! ./pages/currently-playing/currently-playing.module */ "AMjd")).then(m => m.CurrentlyPlayingPageModule),
        canLoad: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
    },
    {
        path: 'played',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-played-played-module */ "pages-played-played-module").then(__webpack_require__.bind(null, /*! ./pages/played/played.module */ "Gt4F")).then(m => m.PlayedPageModule),
        canLoad: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
    },
    {
        path: 'top-games',
        loadChildren: () => Promise.all(/*! import() | pages-top-games-top-games-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-top-games-top-games-module")]).then(__webpack_require__.bind(null, /*! ./pages/top-games/top-games.module */ "mxlt")).then(m => m.TopGamesPageModule)
    },
    {
        path: 'settings',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-settings-settings-module */ "pages-settings-settings-module").then(__webpack_require__.bind(null, /*! ./pages/settings/settings.module */ "yPrK")).then(m => m.SettingsPageModule)
    },
    {
        path: 'forget-password',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-forget-password-forget-password-module */ "pages-forget-password-forget-password-module").then(__webpack_require__.bind(null, /*! ./pages/forget-password/forget-password.module */ "m6ud")).then(m => m.ForgetPasswordPageModule)
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "ynWL":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: transparent;\n}\nion-toolbar ion-title {\n  padding: 0 0 20px 0;\n  font-weight: 900;\n  color: var(--ion-color-primary);\n}\nion-toolbar .menu-circle {\n  z-index: -1;\n  width: 100%;\n  height: 200px;\n  background: var(--ion-color-secondary);\n  position: absolute;\n  border-radius: 0 0 40% 40%;\n  margin-left: auto;\n  margin-right: auto;\n  left: 0;\n  right: 0;\n  bottom: 0;\n}\nion-toolbar .image-container {\n  width: 100%;\n  height: 100px;\n  text-align: center;\n  padding-top: 20px;\n  padding-bottom: 20px;\n}\nion-toolbar .image-container img {\n  display: inline-block;\n  height: 70px;\n  width: 70px;\n  vertical-align: middle;\n  border-radius: 50%;\n  border: 3px solid var(--ion-color-primary);\n}\nion-toolbar .image-container .helper {\n  display: inline-block;\n  vertical-align: middle;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7QUFDSjtBQUNJO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtFQUNBLCtCQUFBO0FBQ1I7QUFFSTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHNDQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7QUFBUjtBQUdJO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7QUFEUjtBQUdRO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQ0FBQTtBQURaO0FBSVE7RUFDSSxxQkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtBQUZaIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFye1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuXHJcbiAgICBpb24tdGl0bGV7XHJcbiAgICAgICAgcGFkZGluZzogMCAwIDIwcHggMDtcclxuICAgICAgICBmb250LXdlaWdodDogOTAwO1xyXG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICB9XHJcblxyXG4gICAgLm1lbnUtY2lyY2xle1xyXG4gICAgICAgIHotaW5kZXg6IC0xO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMjAwcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDAgMCA0MCUgNDAlO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgICAgICBsZWZ0OiAwO1xyXG4gICAgICAgIHJpZ2h0OiAwO1xyXG4gICAgICAgIGJvdHRvbTogMDtcclxuICAgIH1cclxuXHJcbiAgICAuaW1hZ2UtY29udGFpbmVye1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG5cclxuICAgICAgICBpbWd7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgICAgICAgICB3aWR0aDogNzBweDtcclxuICAgICAgICAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgICAgICBib3JkZXI6IDNweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuaGVscGVye1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iXX0= */");

/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "a3Wg");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map